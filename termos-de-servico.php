<!DOCTYPE html>
<html lang="pt">
    
<head>
	<title>TERMOS DE SERVIÇO | YouMeme - BRHUEHUE</title>

    <meta name="keywords" content="Memes, menes, trocadilhos, piadas, frases, you, meme, youmeme, site de memes, criar meme, BR">
    <meta name="description" content="Crie uma conta ou entre no YouMeme. Use sua criatividade para criar e compartilhar seus memes com seus amigos. Compartilhe fotos e vídeos!" >
    
	<meta name="viewport" content="width-width=device, initial-scale=1.0">
    
	<link rel="stylesheet" type="text/css" href="css/estilo-padrao.css">
    <link rel="stylesheet" type="text/css" href="css/estilo-formularios.css">
    <link rel="stylesheet" type="text/css" href="css/estilo-mobile.css">
    <link rel="shortcut icon" href="imagens/Icon_Site.png" type="image/x-icon" >
    
</head>

<body>

    <section id="cabecalho">
        <a href="./">
            <img src="imagens/logo.png">
            <p>Deixe um recado ou meme criativo!</p>
        </a>
    </section> 
    
    <!-- TERMOS - BOX -->
    <section id="box-info">
        
    <h1 align="center" >TERMOS DE SERVIÇO  | YOU MEME</h1><br><hr><br>
        
    <p><b>VISÃO GERAL</b><br>

    Esse site é operado pelo YOU MEME. Em todo o site, os termos “nós”, “nos” e “nosso” se referem ao YOU MEME. O YOU MEME proporciona esse site, incluindo todas as informações, ferramentas e serviços disponíveis deste site para você, o usuário, com a condição da sua aceitação de todos os termos, condições, políticas e avisos declarados aqui.<br><br>

    Ao visitar nosso site você está utilizando nossos “Serviços”. Consequentemente, você  concorda com os seguintes termos e condições (“Termos de serviço”, “Termos”), incluindo os termos e condições e políticas adicionais mencionados neste documento e/ou disponíveis por hyperlink. Esses Termos de serviço se aplicam a todos os usuários do site, incluindo, sem limitação, os usuários que são navegadores e/ou contribuidores de conteúdo.<br><br>

    Por favor, leia esses Termos de serviço cuidadosamente antes de acessar ou utilizar o nosso site. Ao acessar ou usar qualquer parte do site, você concorda com os Termos de serviço. Se você não concorda com todos os termos e condições desse acordo, então você não pode acessar o site ou usar quaisquer serviços. Se esses Termos de serviço são considerados uma oferta, a aceitação é expressamente limitada a esses Termos de serviço.<br><br>

    Quaisquer novos recursos ou ferramentas que forem adicionados ao site atual também devem estar sujeitos aos Termos de serviço. Você pode revisar a versão mais atual dos Termos de serviço quando quiser nesta página. Reservamos o direito de atualizar, alterar ou trocar qualquer parte desses Termos de serviço ao publicar atualizações e/ou alterações no nosso site. É sua responsabilidade verificar as alterações feitas nesta página periodicamente. Seu uso contínuo ou acesso ao site após a publicação de quaisquer alterações constitui aceitação de tais alterações.<br><br>

    <b>SEÇÃO 1 – CENCURAS</b><br>

    Você não deve usar nossos produtos para qualquer fim ilegal ou não autorizado. Você também não pode, ao usufruir deste Serviço, violar quaisquer leis em sua jurisdição (incluindo, mas não limitado, a leis de direitos autorais).<br><br>

    Você não deve transmitir nenhum vírus ou qualquer código de natureza destrutiva.<br><br>

    Violar qualquer um dos Termos tem como consequência a rescisão imediata dos seus Serviços.<br><br>


    <b>SEÇÃO 2 - CONDIÇÕES GERAIS</b><br>

    Reservamos o direito de recusar o serviço a qualquer pessoa por qualquer motivo a qualquer momento.<br><br>

    Você entende que o seu conteúdo pode ser transferido por meio de criptografia e pode: (a) ser transmitido por várias redes; e (b) sofrer alterações para se adaptar e se adequar às exigências técnicas de conexão de redes ou dispositivos.<br><br>

    Você concorda em não reproduzir, duplicar, copiar ou explorar qualquer parte do Serviço, uso do Serviço, acesso ao Serviço, ou qualquer contato no site através do qual o serviço é fornecido, sem nossa permissão expressa por escrito.<br><br>

    Os títulos usados nesse acordo são incluídos apenas por conveniência e não limitam ou  afetam os Termos.<br><br>


    <b>SEÇÃO 3 - PRECISÃO, INTEGRIDADE E ATUALIZAÇÃO DAS INFORMAÇÕES</b><br>

    Não somos responsáveis por informações disponibilizadas nesse site que não sejam precisas, completas ou atuais. O material desse site é fornecido apenas para fins humorísticos e não deve ser usado como a única base para tomar decisões sem consultar fontes de informações primárias, mais precisas, mais completas ou mais atuais. Qualquer utilização do material desse site é por sua conta e risco.<br><br>

    Esse site pode conter certas informações históricas. As informações históricas podem não ser atuais e são fornecidas apenas para comédia. Reservamos o direito de modificar o conteúdo desse site a qualquer momento, mas nós não temos obrigação de atualizar nenhuma informação em nosso site. Você concorda que é de sua responsabilidade monitorar alterações no nosso site.<br><br>

    <b>SEÇÃO 4 - MODIFICAÇÕES DO SERVIÇO E PREÇOS</b><br>

    Os preços dos nossos produtos são sujeitos a alterações sem notificação.<br><br>

    Reservamos o direito de, a qualquer momento, modificar ou descontinuar o Serviço (ou qualquer parte ou conteúdo do mesmo) sem notificação em qualquer momento.<br><br>

    Não nos responsabilizados por você ou por qualquer terceiro por qualquer modificação, suspensão ou descontinuação do Serviço.<br><br>

    <b>SEÇÃO 5 - FERRAMENTAS OPCIONAIS</b><br>

    Podemos te dar acesso a ferramentas de terceiros que não monitoramos e nem temos qualquer controle.<br><br>

    Você reconhece e concorda que nós fornecemos acesso a tais ferramentas ”como elas são” e “conforme a disponibilidade” sem quaisquer garantias, representações ou condições de qualquer tipo e sem qualquer endosso. Não nos responsabilizamos de forma alguma pelo seu uso de ferramentas opcionais de terceiros.<br><br>

    Qualquer uso de ferramentas opcionais oferecidas através do site é inteiramente por sua conta e risco e você se familiarizar e aprovar os termos das ferramentas que são fornecidas por fornecedor(es) terceiro(s).<br><br>

    Também podemos, futuramente, oferecer novos serviços e/ou recursos através do site (incluindo o lançamento de novas ferramentas e recursos). Tais recursos e/ou serviços novos também devem estar sujeitos a esses Termos de serviço.<br><br>

    <b>SEÇÃO 6 - LINKS DE TERCEIROS</b><br>

    Certos conteúdos e serviços disponíveis pelo nosso Serviço podem incluir materiais de terceiros.<br><br>

    Os links de terceiros nesse site podem te direcionar para sites de terceiros que não são afiliados a nós. Não nos responsabilizamos por examinar ou avaliar o conteúdo ou precisão. Não garantimos e nem temos obrigação ou responsabilidade por quaisquer materiais ou sites de terceiros, ou por quaisquer outros materiais, produtos ou serviços de terceiros.<br><br>

    Não somos responsáveis por quaisquer danos ou prejuízos relacionados com o uso serviços, recursos, conteúdo, ou quaisquer outras transações feitas em conexão com quaisquer sites de terceiros. Por favor, revise com cuidado as políticas e práticas de terceiros e certifique-se que você as entende antes de efetuar qualquer transação. As queixas, reclamações, preocupações ou questões relativas a produtos de terceiros devem ser direcionadas ao terceiro.<br><br>

    <b>SEÇÃO 7 - COMENTÁRIOS, FEEDBACK, ETC. DO USUÁRIO</b><br>

    Podemos, mas não temos a obrigação, de monitorar, editar ou remover conteúdo que nós determinamos a nosso próprio critério ser contra a lei, ofensivo, ameaçador, calunioso, difamatório, pornográfico, obsceno ou censurável ou que viole a propriedade intelectual de terceiros ou estes Termos de serviço.<br><br>

    Você concorda que seus comentários não violarão qualquer direito de terceiros, incluindo direitos autorais, marcas registradas, privacidade, personalidade ou outro direito pessoal ou de propriedade. Você concorda que os seus comentários não vão conter material difamatório, ilegal, abusivo ou obsceno. Eles também não conterão nenhum vírus de computador ou outro malware que possa afetar a operação do Serviço ou qualquer site relacionado. Você não pode usar um endereço de e-mail falso, fingir ser alguém diferente de si mesmo, ou de outra forma enganar a nós ou terceiros quanto à origem de quaisquer comentários. Você é o único responsável por quaisquer comentários que você faz e pela veracidade deles. Nós não assumimos qualquer responsabilidade ou obrigação por quaisquer comentários publicados por você ou por qualquer terceiro.<br><br>

    <b>SEÇÃO 8 - INFORMAÇÕES PESSOAIS</b><br>

    O envio de suas informações pessoais através do perfil é regido pela nossa <a href="politica-de-privacidade" target="_blank" >Política de privacidade.</a><br><br>

    <b>SEÇÃO 9 - USOS PROIBIDOS</b><br>

    Além de outras proibições, conforme estabelecido nos Termos de serviço, você está proibido de usar o site ou o conteúdo para: (a) fins ilícitos; (b) solicitar outras pessoas a realizar ou participar de quaisquer atos ilícitos; (c) violar quaisquer regulamentos internacionais, provinciais, estaduais ou federais, regras, leis ou regulamentos locais; (d) infringir ou violar nossos direitos de propriedade intelectual ou os direitos de propriedade intelectual de terceiros; (e) para assediar, abusar, insultar, danificar, difamar, caluniar, depreciar, intimidar ou discriminar com base em gênero, orientação sexual, religião, etnia, raça, idade, nacionalidade ou deficiência; (f) apresentar informações falsas ou enganosas; (g) fazer o envio ou transmitir vírus ou qualquer outro tipo de código malicioso que será ou poderá ser utilizado para afetar a funcionalidade ou operação do Serviço ou de qualquer site relacionado, outros sites, ou da Internet; (h) coletar ou rastrear as informações pessoais de outras pessoas; (i) para enviar spam, phishing, pharm, pretext, spider, crawl, ou scrape; (j) para fins obscenos ou imorais; ou (k) para interferir ou contornar os recursos de segurança do Serviço ou de qualquer site relacionado, outros sites, ou da Internet. Reservamos o direito de rescindir o seu uso do Serviço ou de qualquer site relacionado por violar qualquer um dos usos proibidos.<br><br>

    <b>SEÇÃO 10 - ISENÇÃO DE RESPONSABILIDADE DE GARANTIAS; LIMITAÇÃO DE RESPONSABILIDADE</b><br>

    Nós não garantimos, representamos ou justificamos que o seu uso do nosso serviço será pontual, seguro, sem erros ou interrupções.<br><br>

    Não garantimos que os resultados que possam ser obtidos pelo uso do serviço serão precisos ou confiáveis.<br><br>

    Você concorda que de tempos em tempos, podemos remover o serviço por períodos indefinidos de tempo ou cancelar a qualquer momento, sem te notificar.<br><br>

    Em nenhuma circunstância o YOU MEME, nossos diretores, oficiais, funcionários, afiliados, agentes, contratantes, estagiários, fornecedores, prestadores de serviços ou licenciadores serão responsáveis por qualquer prejuízo, perda, reclamação ou danos diretos, indiretos, incidentais, punitivos, especiais ou consequentes de qualquer tipo, incluindo, sem limitação, lucros cessantes, perda de dados, ou quaisquer danos semelhantes, seja com base em contrato, ato ilícito (incluindo negligência), responsabilidade objetiva ou de outra forma, decorrentes do seu uso de qualquer um dos serviços ou quaisquer produtos adquiridos usando o serviço, ou para qualquer outra reclamação relacionada de alguma forma ao seu uso do serviço ou qualquer produto, incluindo, mas não limitado a, quaisquer erros ou omissões em qualquer conteúdo, ou qualquer perda ou dano de qualquer tipo como resultado do uso do serviço ou qualquer conteúdo publicado, transmitido ou de outra forma disponível através do serviço, mesmo se alertado de tal possibilidade. Como alguns estados ou jurisdições não permitem a exclusão ou a limitação de responsabilidade por danos consequentes ou incidentais, em tais estados ou jurisdições, a nossa responsabilidade será limitada à extensão máxima permitida por lei.<br><br>

    <b>SEÇÃO 11 - INDEPENDÊNCIA</b><br>

    No caso de qualquer disposição destes Termos de serviço ser considerada ilegal, nula ou ineficaz, tal disposição deve, contudo, ser aplicável até ao limite máximo permitido pela lei aplicável, e a porção inexequível será considerada separada desses Termos de serviço. Tal determinação não prejudica a validade e aplicabilidade de quaisquer outras disposições restantes.<br><br>

    <b>SEÇÃO 12 - RESCISÃO</b><br>

    As obrigações e responsabilidades das partes incorridas antes da data de rescisão devem continuar após a rescisão deste acordo para todos os efeitos.<br><br>

    Estes Termos de Serviço estão em vigor, a menos que seja rescindido por você ou por nós. Você pode rescindir estes Termos de serviço a qualquer momento, notificando-nos que já não deseja utilizar os nossos serviços, ou quando você deixar de usar o nosso site.<br><br>

    Se em nosso critério exclusivo você não cumprir com qualquer termo ou disposição destes Termos de serviço, nós também podemos rescindir este contrato a qualquer momento sem aviso prévio e você ficará responsável por todas as quantias devidas até a data da rescisão; também podemos lhe negar acesso a nossos Serviços (ou qualquer parte deles).<br><br>


    <b>SEÇÃO 13 - ACORDO INTEGRAL</b><br>

    Caso não exerçamos ou executemos qualquer direito ou disposição destes Termos de serviço, isso não constituirá uma renúncia de tal direito ou disposição.<br><br>

    Estes Termos de serviço e quaisquer políticas ou normas operacionais postadas por nós neste site ou no que diz respeito ao serviço constituem a totalidade do acordo  entre nós. Estes termos regem o seu uso do Serviço, substituindo quaisquer acordos anteriores ou contemporâneos, comunicações e propostas, sejam verbais ou escritos, entre você e nós (incluindo, mas não limitado a quaisquer versões anteriores dos Termos de serviço).<br><br>

    Quaisquer ambiguidades na interpretação destes Termos de serviço não devem ser interpretadas contra a parte que os redigiu.<br><br>

    <b>SEÇÃO 14 - LEGISLAÇÃO APLICÁVEL</b><br>

    Esses Termos de serviço que nós lhe fornecemos os Serviços devem ser regidos e interpretados de acordo com as leis de youmeme.ml, Juazeiro do Norte, CE, Brazil.<br><br>

    <b>SEÇÃO 15 - ALTERAÇÕES DOS TERMOS DE SERVIÇO</b><br>

    Você pode rever a versão mais atual dos Termos de serviço a qualquer momento nessa página.<br><br>

    Reservamos o direito, a nosso critério, de atualizar, modificar ou substituir qualquer parte destes Termos de serviço ao publicar atualizações e alterações no nosso site. É sua responsabilidade verificar nosso site periodicamente. Seu uso contínuo ou acesso ao nosso site ou ao Serviço após a publicação de quaisquer alterações a estes Termos de serviço constitui aceitação dessas alterações.<br><br>
        
    <b>SEÇÃO 16 - CONTEÚDO DO SITE</b><br>
    Declaramos que a equipe do You Meme tem como intenção criar um ambiente cômico para todos os públicos, sendo assim não nos responsabilizamos por quaisquer formas de difamação de terceiros através de post's, nomes, ícones ou outros meios de mídias, incluindo os dados apresentados no referente site.<br><br>

    <b>SEÇÃO 17 - INFORMAÇÕES DE CONTATO</b><br>

    As perguntas sobre os Termos de serviço devem ser enviadas para nós através do nosso e-mail de contato: <br>
    <b>youmemebr@gmail.com.</b>
        
        
    <br><br>Atenciosamente,<br>Equipe YouMeme.</p>
    </section>
    
    <footer>
        <p><a href="https://www.instagram.com/jeysson_mourasz/" target="_blank" >CEO</a> | <a href="https://www.instagram.com/marcosnemrode/" target="_blank" >COO</a> | <a href="termos-de-servico"  >TERMOS</a> | <a href="politica-de-privacidade" >PRIVACIDADE</a> YouMeme © 2017</p>
    </footer>
    
</body>
 
</html>