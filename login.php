<?php

    require_once('conexao.php');
    session_start();

    include("proteger-user.php");
    sairpagina();

    setlocale( LC_ALL, 'pt_BR', 'pt_BR.iso-8859-1', 'pt_BR.utf-8', 'portuguese' ); 
    date_default_timezone_set( 'America/Sao_Paulo' );
    $exibirdata = date('Y-m-d');
    $exibirhora = date('H:i:s');

?>


<!DOCTYPE html>
<html lang="pt">
    
<head>
	<title>YouMeme - LOGIN</title>

    <meta name="keywords" content="Memes, menes, trocadilhos, piadas, frases, you, meme, youmeme, YouMeme, site de memes, criar meme, BR">
    <meta name="description" content="Crie uma conta ou entre no YouMeme. Use sua criatividade para criar e compartilhar seus memes com seus amigos. Compartilhe fotos e vídeos!" >
    
	<meta name="viewport" content="width-width=device, initial-scale=1.0">
    
	<link rel="stylesheet" type="text/css" href="css/estilo-padrao.css">
    <link rel="stylesheet" type="text/css" href="css/estilo-formularios.css">
    <link rel="stylesheet" type="text/css" href="css/estilo-mobile.css">
    <link rel="shortcut icon" href="imagens/Icon_Site.png" type="image/x-icon" >
    
    <script async src="js/jquery-3.2.1.min.js"></script>
    <script async src="js/funcoes-site.js"></script>
    
</head>

<body>
    
    <!-- SELO -->
    <section class="selo">
        <a href="https://www.facebook.com/youmemeBR/" target="_blank" ><img src="imagens/Selo.png"></a>
    </section>
    
    <!-- GERAL - BOX -->
    <section id="box-geral">

        <!-- LOGIN - BOX -->
        <section id="box-login" class="box-design" >
            <a href="./"><img src="imagens/logo.png"></a>
            <form method="POST">
                <input type="text" name="usuario" placeholder="Nick ou E-mail" required >
                <input type="password" name="senha" placeholder="Senha" required >
                <input class="botao-postar" type="submit" name="bt-login" value="ENTRAR">
            </form>
            <form action="cadastrar" method="POST">
                <input class="botao-postar" type="submit" name="bt-cadastro" value="CRIAR CONTA">
            </form>
            <br><a href="recuperar">Esqueceu sua senha?</a><br><br>
        </section>
        
    </section>
    
    <!-- GERAL - BOX -->
    <section id="box-geral">

        <!-- USUER LOGIN - BOX -->
        <section id="box-user-login" class="box-design" >
            <h1>NOVOS MEMBROS!</h1><hr>
            <?php
            
                $dadosVisitante = $PDO->query("SELECT * FROM usuarios ORDER BY id DESC LIMIT 0,4 ");
            
                if($dadosVisitante->rowCount() > 0 ){

                    foreach($dadosVisitante->fetchAll() as $Visitante_dados){
                        $PublicarPERFIL_Visitante = $Visitante_dados['perfil'];
                        $PublicarNOME_Visitante = $Visitante_dados['nome'];
            ?>
            <a href="<?php echo $PublicarNOME_Visitante; ?>"><img src="<?php echo $PublicarPERFIL_Visitante; ?>"></a>
            <?php } } else { ?>
            <h1>Nenhum membro...</h1>
            <?php } ?>
        </section>
        
    </section>
    
    <footer>
        <p><a href="https://www.instagram.com/jeysson_mourasz/" target="_blank" >CEO</a> | <a href="https://www.instagram.com/marcosnemrode/" target="_blank" >COO</a> | <a href="termos-de-servico" target="_blank" >TERMOS</a> | <a href="politica-de-privacidade" target="_blank" >PRIVACIDADE</a>. YouMeme © 2017</p>
    </footer>
    
</body>
 
</html>


<?php

    if(isset($_POST['bt-login'])){
        
        $nome = addslashes($_POST["usuario"]);
        $senha = md5($_POST["senha"]);
    
        $selecionar_user = $PDO->query("SELECT * FROM usuarios WHERE (email='$nome' OR nome='$nome') AND senha='$senha' ");
        
        if($selecionar_user->rowCount() > 0){

            foreach($selecionar_user->fetchAll() as $dadosM){
                $PublicarNOME = $dadosM['nome'];
            }

            session_start();
            $_SESSION["Nome"] = $PublicarNOME;
            $PDO->query("UPDATE usuarios SET logoff='$exibirdata' WHERE nome='$nome' ");
            echo "<script> location.href='./' </script>";

        }
        else {
            echo "<script> alert('Usuário ou senha incorreto!'); </script>";
        }

    }

?>