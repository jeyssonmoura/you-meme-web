<?php

    require_once('conexao.php');
    session_start();

    include("proteger-user.php");
    protegerpagina();
    sairpagina();

    $NickUser = ucwords($_SESSION["Nome"]);

    $notifica = $PDO->query("SELECT * FROM posters ORDER BY id DESC LIMIT 0, 10");

    $dadosUser = $PDO->query("SELECT * FROM usuarios WHERE nome='$NickUser' ");

    if($dadosUser->rowCount() > 0 ){

        foreach($dadosUser->fetchAll() as $user_dados){
            $PublicarPERFIL_USER = $user_dados['perfil'];
            $PublicarADM_USER = $user_dados['ADM'];
            $PublicarNOTIFICAR_USER = $user_dados['notificar'];
        }

    }

    setlocale( LC_ALL, 'pt_BR', 'pt_BR.iso-8859-1', 'pt_BR.utf-8', 'portuguese' ); 
    date_default_timezone_set( 'America/Sao_Paulo' );
    $exibirdata = date('Y-m-d');
    $exibirhora = date('H:i:s');

?>

<!DOCTYPE html>
<html lang="pt">
    
<head>
	<title><?php if($PublicarNOTIFICAR_USER>0){ echo "($PublicarNOTIFICAR_USER)"; } ?> YouMeme - BRHUEHUE</title>

    <meta name="keywords" content="Memes, menes, trocadilhos, piadas, frases, you, meme, youmeme, site de memes, criar meme, BR">
    <meta name="description" content="Crie uma conta ou entre no YouMeme. Use sua criatividade para criar e compartilhar seus memes com seus amigos. Compartilhe fotos e vídeos!" >
	<meta name="viewport" content="width-width=device, initial-scale=1.0">
    
    <link rel="stylesheet" type="text/css" href="css/estilo-padrao.css">
    <link rel="stylesheet" type="text/css" href="css/estilo-mobile.css">
    <link rel="stylesheet" type="text/css" href="css/estilo-perfil.css">
    <link rel="stylesheet" type="text/css" href="css/estilo-topicos.css">
    <link rel="stylesheet" type="text/css" href="css/estilo-formularios.css">
    <link rel="stylesheet" type="text/css" href="css/estilo-notificacoes.css">
    
    <link rel="shortcut icon" href="imagens/Icon_Site.png" type="image/x-icon" >
    <script async src="js/jquery-3.2.1.min.js"></script>
    <script async src="js/funcoes-site.js"></script>
    
</head>

<body>
    
    <!-- SELO -->
    <section class="selo">
        <a href="https://www.facebook.com/youmemeBR/" target="_blank" ><img src="imagens/Selo.png"></a>
    </section>
    
    <!-- PERFIl - MENU -->
    <nav class="menu">
        <ul>
            <a href="<?php echo $NickUser ?>"><img class="pefil-img" src="<?php echo $PublicarPERFIL_USER ?>"></a>
            <li><a href="<?php echo $NickUser ?>"><strong class="meu-nick"><?php echo $NickUser ?></strong></a></li>
            <li><form method="get" ><input type="search" name="buscar" placeholder="Buscar..."></form></form></li>
            <!--
           <figure class="pefil-notificacao">
                <a href="perfil.php"><img class="pefil-icons" src="imagens/icon-chat.png"></a>
                <figcaption> 
                    <p>0</p>
                </figcaption>
            </figure> -->

            <figure class="pefil-notificacao">

                <section id="bt-notificacoes-a">
                    <a href="javascript:abrir_notificacoes();"><img class="pefil-icons" src="imagens/icon-notificacao.png"></a>
                </section>

                <section id="bt-notificacoes-f">
                    <a onClick="javascript:fechar_notificacoes();" 
                       href="<?php if($PublicarNOTIFICAR_USER > 0){ echo "?notifique=Notificar"; } else { echo "#"; } ?> "> 
                       <img class="pefil-icons" src="imagens/icon-notificacao.png"></a>
                </section>

                <figcaption> 
                    <p><?php if($PublicarNOTIFICAR_USER>0){ echo $PublicarNOTIFICAR_USER; } ?></p>
                </figcaption>
            </figure>
    

            <!-- NOTIFICAÇÕES - BOX -->
            <section id="popup-notificacoes">
                <h1>NOTIFICAÇÕES <?php if($PublicarNOTIFICAR_USER>0){ echo "($PublicarNOTIFICAR_USER)"; } ?></h1>
                <section class="barra-notificacoes">
                    <?php

                    if(isset($_GET["notifique"]) AND $_GET["notifique"] == "Notificar" && $PublicarNOTIFICAR_USER > 0 ){
                        $VerificarNotificar = 0;
                        $PDO->query("UPDATE usuarios SET notificar='$VerificarNotificar' WHERE nome='$NickUser' ");
                        echo "<script> location.href='index.php'; </script>";
                    }

                    if($notifica->rowCount() > 0 ){

                        foreach($notifica->fetchAll() as $exibir_notifica){
                            
                            $PublicarID = $exibir_notifica['id'];
                            $PublicarIMG = $exibir_notifica['img'];
        
                    ?>
                    <p><img src="<?php if($PublicarIMG!=""){echo $PublicarIMG;} else {echo "imagens/sem-foto.png"; } ?>"><a href="poster?id=<?php echo $PublicarID ?>">Passando aqui para:<br>NOVO MEME!</a></p>
                    <?php } } else { ?>
                    <h1>Nenhuma notificação...</h1>
                    <?php } ?>
                </section>
            </section>
    
            <li>
                <a href="#"><img src="imagens/icon-eng.png"></a>
                <ul>
                    <li><a href="<?php echo $NickUser ?>">PERFIL</a></li>
                    <li><a href="index.php?func=sairpagina">SAIR</a></li>
                </ul>
            </li>
        </ul>
    </nav><br><br>
    
    <section id="cabecalho">
        <a href="./">
            <img src="imagens/logo.png">
            <p>Deixe um recado ou meme criativo!</p>
        </a>
    </section> 
	
	<style>
		#box-mais-tocipos {
			display: none;
		}
		#box-menos-t {
			display: none;
		}
	</style>

    <!-- AVISOS - BOX -->
    <?php $avisos_e = $_GET['aviso-e']; if($avisos_e != ""){ ?>
    <section id="box-avisos" class="cor-erro">
        <h1 class="cor-erro" ><?php echo $avisos_e; ?><h1><a href="javascript:fechar_avisos();">X</a>
    </section>
    <?php } $avisos_s = $_GET['aviso-s']; if($avisos_s != ""){  ?>
    <section id="box-avisos">
        <h1><?php echo $avisos_s; ?><h1><a href="javascript:fechar_avisos();">X</a>
    </section>
    <?php } ?>

    <!-- TOPICOS -->
   <section id="topicos-box" >

        <p align="center">TÓPICOS</p>
        
       <?php

        $Tags = $PDO->query("SELECT DISTINCT(tag) FROM posters");

            if($Tags->rowCount() > 0 ){

                foreach($Tags->fetchAll() as $tag_exibir){
                    $PublicarTAGS = $tag_exibir['tag'];
                    
        ?>
       
        <a href="topicos?tag=<?php $TagExibir = substr($PublicarTAGS, 1); echo $TagExibir; ?>"><?php echo $PublicarTAGS; ?></a>
       
        <?php } } ?>

   </section>
    
    <!-- POSTAGEM - LISTA - TOPICOS - BOX -->
    <?php

    $tag = $_GET['tag'];

    if(empty($_GET['pg'])){}else {
        $pg = $_GET['pg'];
    }

    if(isset($pg)){
        $pg = $_GET['pg'];
    }
    else {
        $pg = 1;
    }

    $quantidade = 5;
    $inicio = ($pg*$quantidade)- $quantidade;
    
    $verificar = $PDO->query("SELECT * FROM posters WHERE tag LIKE '%$tag%' ORDER BY id DESC LIMIT $inicio, $quantidade");
    $total_poster = $PDO->query("SELECT * FROM posters");
    $maxpost = $total_poster->rowCount();
    
    if($buscar != ""){
        echo "<p class='busca-result' >RESULTADO PARA: $buscar</p>";
    }

    if($verificar->rowCount() > 0 ){

        foreach($verificar->fetchAll() as $exibir){
            
            $PublicarID = $exibir['id'];
            $PublicarNICK = $exibir['nick']; 
            $PublicarPOSTER = $exibir['poster'];
            $PublicarDATA = $exibir['data'];
            $PublicarHORA = $exibir['hora'];
            $PublicarIMG = $exibir['img'];
            $PublicarVIDEO = $exibir['video'];
            $PublicarGOSTEI = $exibir['gostei'];
            $PublicarNAOGOSTEI = $exibir['naogostei'];
            $PublicarNAOGOSTEI = $exibir['naogostei'];
            $PublicarTAG = $exibir['tag'];
            
            $verificar_perfil = $PDO->query("SELECT * FROM usuarios WHERE nome='$PublicarNICK' ");
            
            if($verificar_perfil->rowCount() > 0 ){

            foreach($verificar_perfil->fetchAll() as $perfil_exibir){
                $PublicarPERFIL_VISITANTE = $perfil_exibir['perfil'];
                $PublicarADM_VISITANTE = $perfil_exibir['ADM'];
            ?>
    
            <section id="box-postagens">
                <?php if($PublicarNICK != $NickUser){ ?>
                    <a class="link-perfil" href="<?php echo $PublicarNICK ?>" > 
                        
                        <figure class="admin-icon">
                            <img class="imagem-post" src="<?php echo $PublicarPERFIL_VISITANTE ?>">
                            <?php if($PublicarADM_VISITANTE == 1){ ?>
                                <figcaption> 
                                        <img class="icon-perfil" src="imagens/icon-admin.png">
                                </figcaption>
                            <?php } ?>
                        </figure>
                        
                    </a>
                <?php } else { ?>
                    <a class="link-perfil" href="<?php echo $NickUser ?>" >
                         <figure class="admin-icon">
                            <img class="imagem-post" src="<?php echo $PublicarPERFIL_VISITANTE ?>">
                            <?php if($PublicarADM_VISITANTE == 1){ ?>
                                <figcaption> 
                                        <img class="icon-perfil" src="imagens/icon-admin.png">
                                </figcaption>
                            <?php } ?>
                        </figure>
                    </a>
                <?php } ?>
                
            <?php } } else { ?>
            <section id="box-postagens">
            <img class="imagem-post" src="imagens/sem-foto.png">

            <?php } ?>
            <font color="#fc9300" face="Arial, sans-serif">

            <?php if($PublicarNICK != $NickUser){ ?>
                <a class="link-perfil" href="perfil.php?nick=<?php echo $PublicarNICK ?>" ><?php echo $PublicarNICK ?></a>
                <?php } else{ ?>
                <a class="link-perfil" href="perfil.php" ><?php echo $PublicarNICK ?></a>
            <?php } ?>

            - <?php echo $PublicarDATA ?> às <?php echo $PublicarHORA ?></font>
                <h1><a href="topicos?tag=<?php $TagExibirPost = substr($PublicarTAG, 1); echo $TagExibirPost; ?>"><?php echo $PublicarTAG; ?></a></h1>
            <?php 

            if($PublicarPOSTER!="" && $PublicarIMG!=""){ ?>

                <figure class="meme-legenda">
                    <a href="poster?id=<?php echo $PublicarID ?>" ><img src="<?php echo $PublicarIMG ?>"></a>
                        <figcaption> 
                            <h3><?php echo $PublicarPOSTER ?></h3> 
                        </figcaption>
                </figure>
                
            <?php }else{

            if($PublicarPOSTER != "" && $PublicarVIDEO ==""){
                $chars = array("[E1]", "[E2]", "[E3]", "[E4]", "[E5]", "[E6]", "[E7]", "[E8]", "[E9]", "[E10]", "[E11]", "[E12]", "[E13]", "[E14]", "[E15]", "[E16]", "[E17]", "[E18]", "[E19]");
                $icone = array("<img src='emojis/EMOJI1.png'1>" ,"<img src='emojis/EMOJI2.png'2>" ,"<img src='emojis/EMOJI3.png'3>" ,"<img src='emojis/EMOJI4.png'4>" ,"<img src='emojis/EMOJI5.png'5>" ,"<img src='emojis/EMOJI6.png'6>" ,"<img src='emojis/EMOJI7.png'7>" ,"<img src='emojis/EMOJI8.png'8>" ,"<img src='emojis/EMOJI9.png'9>" ,"<img src='emojis/EMOJI10.png'10>" ,"<img src='emojis/EMOJI11.png'11>" ,"<img src='emojis/EMOJI12.png'12>" ,"<img src='emojis/EMOJI13.png'13>" ,"<img src='emojis/EMOJI14.png'14>" ,"<img src='emojis/EMOJI15.png'15>" ,"<img src='emojis/EMOJI16.png'16>" ,"<img src='emojis/EMOJI17.png'17>" ,"<img src='emojis/EMOJI18.png'18>" ,"<img src='emojis/EMOJI19.png'19>");
                $nova_frase = str_replace($chars, $icone, $PublicarPOSTER);
                ?><a href="poster?id=<?php echo $PublicarID ?>" ><p><?php echo $nova_frase; ?></p></a>
            <?php } ?>

            <?php if($PublicarIMG != ""){ ?>
                
                <section class="img-poster">
                    <a href="poster?id=<?php echo $PublicarID ?>" ><img src="<?php echo $PublicarIMG ?>"></a>
                </section>
                
            <?php } ?>

            <?php if($PublicarVIDEO != ""){ ?>
            <figure class="poster-video-play" >
                <a href="poster?id=<?php echo $PublicarID ?>" >
                <video class="video-poster" id="video-poster-<?php echo $exibir['id']?>"  name="media">
                    <source src="<?php echo $PublicarVIDEO ?>" type="video/mp4">
                    </video></a>
                
                <figcaption>
                    
                <?php if($PublicarPOSTER != ""){ ?>
                    <h3 class="meme-legenda-video" ><?php echo $PublicarPOSTER ?></h3> 
                <?php } ?>
                    
                <script>
                 /* ------------ VIDEOS PLAY ------------ */
                function PlayVideo<?php echo $exibir['id']; ?> () {
                    document.getElementById('video-poster-<?php echo $exibir['id']; ?>').play();
                    document.getElementById('video-play-<?php echo $exibir['id']; ?>').style.display = 'none';
                    document.getElementById('video-pause-<?php echo $exibir['id']; ?>').style.display = 'block';
                }
                function PauseVideo<?php echo $exibir['id']; ?> () {
                    document.getElementById('video-poster-<?php echo $exibir['id']; ?>').pause();
                    document.getElementById('video-play-<?php echo $exibir['id']; ?>').style.display = 'block';
                    document.getElementById('video-pause-<?php echo $exibir['id']; ?>').style.display = 'none';
                }
                </script>

                <style>
                    #video-pause-<?php echo $PublicarID ?> {
                        display: none;
                    }
                    #video-play-<?php echo $PublicarID ?> {
                        display: block;
                    }
                </style>

                <a id="video-play-<?php echo $exibir['id']?>" href="javascript:PlayVideo<?php echo $exibir['id']?>();" ><img src="imagens/play-video.png" ></a>
                <a id="video-pause-<?php echo $exibir['id']?>" href="javascript:PauseVideo<?php echo $exibir['id']?>();" ><img src="imagens/pause-video.png" ></a>

                </figcaption>
            </figure>
            <?php } } ?>

            <nav class="menu-poster">
                <ul>
                    <li><a href="?funcao=gostei&id=<?php echo $PublicarID ?>"><img src="imagens/gostei-click.png"> <?php echo $PublicarGOSTEI ?></a></li>
                    <li><a href="?funcao=naogostei&id=<?php echo $PublicarID ?>"><img src="imagens/nao-gostei-click.png"> <?php echo $PublicarNAOGOSTEI ?></a></li>
                    <li><a class="bt-facebook" href="http://www.facebook.com/sharer/sharer.php?u=http://www.youmeme.ml/poster?id=<?php echo $PublicarID ?>" target="_blank" class="share-btn facebook"><img src="imagens/bt-facebook.png"></a></li>
                    <?php if($PublicarADM_USER == 1 || $PublicarNICK == $NickUser){ ?>
                    <li><a href="javascript:func();" onclick="excluir(<?php echo $exibir['id']; ?>)" ><img src="imagens/icon-excluir.png"></a></li>
                    <?php } ?>
                </ul>
            </nav>
                
                <script>

                function IdPost<?php echo $PublicarID ?> () {
                    document.getElementById('id-post-<?php echo $PublicarID ?>').value = '<?php echo $PublicarID ?>';
                }
                
            </script>
                
            <!-- COMENTÁRIOS -->
            <section id="box-comentarios">
                
                <?php $TodosComentariosAtual = $PDO->query("SELECT * FROM comentarios WHERE id_post='$PublicarID' "); ?>
                <h1><?php echo $TodosComentariosAtual->rowCount(); ?> comentários</h1><hr>
                
                <form method="post" >
                    <a class="link-perfil" href="<?php echo $PublicarNOME_COMENTARIO ?>" >
                         <figure class="admin-icon">
                            <img class="avatar-perfil" src="<?php echo $PublicarPERFIL_USER; ?>">
                            <?php if($PublicarADM_USER == 1){ ?>
                                <figcaption> 
                                    <img class="icon-perfil" src="imagens/icon-admin.png">
                                </figcaption>
                            <?php } ?>
                        </figure>
                    </a>
                    <textarea name="box-comentar" placeholder="Comentar..." ></textarea>
                    <input class="ocultar" type="text" id="id-post-<?php echo $PublicarID ?>" name="id-post">
                    <input class="botao-comentar" type="submit" name="bt-comentar" onclick="javascript:IdPost<?php echo $PublicarID ?>();" value="COMENTAR" >
                </form>

                <?php 

                    $TodosComentarios = $PDO->query("SELECT * FROM comentarios WHERE id_post='$PublicarID' ORDER BY id DESC LIMIT 0,3 ");

                    if($TodosComentarios->rowCount() > 0 ){

                    foreach($TodosComentarios->fetchAll() as $exibir_comentarios){
                        $PublicarID_COMENTARIO = $exibir_comentarios['id'];
                        $PublicarNOME_COMENTARIO = $exibir_comentarios['nick_comentario'];
                        $PublicarDATA_COMENTARIO = $exibir_comentarios['data']; 
                        $PublicarHORA_COMENTARIO = $exibir_comentarios['hora']; 
                        $Publicar_COMENTARIO = $exibir_comentarios['comentario']; 
                        
                        $TodosComentariosIMG = $PDO->query("SELECT * FROM usuarios WHERE nome='$PublicarNOME_COMENTARIO' ");
                        
                        foreach($TodosComentariosIMG->fetchAll() as $exibir_comentarios_img){
                            $PublicarPERFIL_COMENTARIO = $exibir_comentarios_img['perfil'];
                            $PublicarADM_COMENTARIO = $exibir_comentarios_img['ADM'];
                        
                    ?>
                
                <section class="comentarios-post">
                    <a class="link-perfil" href="<?php echo $PublicarNOME_COMENTARIO ?>" >
                         <figure class="admin-icon">
                            <img class="avatar-perfil" src="<?php echo $PublicarPERFIL_COMENTARIO; ?>">
                            <?php if($PublicarADM_COMENTARIO == 1){ ?>
                                <figcaption> 
                                    <img class="icon-perfil" src="imagens/icon-admin.png">
                                </figcaption>
                            <?php } ?>
                        </figure>
                    </a>
                    <p><a href="<?php echo $PublicarNOME_COMENTARIO ?>"><?php echo $PublicarNOME_COMENTARIO ?></a><font color="#333" size="2"> - <?php echo $PublicarDATA_COMENTARIO ?> às <?php echo $PublicarHORA_COMENTARIO ?></font><br>
                    <?php echo $Publicar_COMENTARIO; ?></p>
                    <?php if($PublicarADM == 1 || $PublicarNOME_COMENTARIO == $NickUser){ ?>
                    <a class="excluir-comentario" href="javascript:func();" onclick="excluirComentario(<?php echo $PublicarID_COMENTARIO; ?>)" ><img src="imagens/icon-excluir.png"></a>
                    <?php } ?>
                </section>
                
                <?php } } ?>
                
                <?php if($TodosComentarios->rowCount() == 3){ ?>
                <section class="link-pag-recados">
                    <a href="poster?id=<?php echo $PublicarID ?>" >CARREGAR TODOS...</a>
                </section><br>
                <?php } } ?>

            </section>
                
        </section>
            
    <?php
        }
    }
    else{
        ?>
        <section id="box-postagens">
            <p>Seja o(a) primeiro(a) à publicar!</p>
        </section>
       <?php
    }
    ?>
           
                
    <!-- PAGINACAO -->
    <section class="link-pag">
    <br><a href="topicos.php?&pg=1">INÍCIO</a>  
    <?php
    
        if($maxpost <= $quantidade){} else {
            
            $links = 5;
            $paginas = ceil($maxpost/$quantidade);
    
            if(isset($i)){} else{
                $i = 1;
            }
            
        }

        if(isset($_GET['pg'])){
            $num_pg = $_GET['pg'];
        }
    
        for($i = $pg-$links; $i <= $pg-1; $i++){
            if($i <= 0){} else { ?>
            <a href="topicos.php?&pg=<?php echo $i; ?>"><?php echo $i; ?></a> 
        <?php } } ?><a href="topicos.php?&pg=<?php echo $i; ?>"><?php echo $i; ?></a>
                
        <?php for($i = $pg+1; $i <= $pg+$links; $i++){
            if($i > $paginas){} else { ?>
                <a href="topicos.php?&pg=<?php echo $i; ?>"><?php echo $i; ?></a>
        <?php } } ?>
    </section>
    
    <footer>
        <p><a href="https://www.instagram.com/jeysson_mourasz/" target="_blank" >CEO</a> | <a href="https://www.instagram.com/marcosnemrode/" target="_blank" >COO</a> | <a href="termos-de-servico" target="_blank" >TERMOS</a> | <a href="politica-de-privacidade" target="_blank" >PRIVACIDADE</a> YouMeme © 2017</p>
    </footer>
    
</body>

    
</html>


<?php

    if(isset($_POST["bt-comentar"])){

        $meuidpost = $_POST["id-post"];
        $comentario = $_POST['box-comentar'];
        
        if($comentario != ""){
        
            $envarcomentario = $PDO->query("INSERT INTO comentarios (id_post, nick_comentario, data, hora, comentario) VALUES ('$meuidpost', '$NickUser' ,'$exibirdata', '$exibirhora', '$comentario' )");

            if($envarcomentario->rowCount() > 0){
                echo "<script> location.href='?aviso-s=Enviado com sucesso!'; </script>";
            }
            else{
                echo "<script> alert('Erro ao enviar!'); </script>";
            }
            
        }
    }

    if(isset($_POST['bt-login'])){
        
        $nome = $_POST["usuario"];
        $senha = md5($_POST["senha"]);
    
        $selecionar_user = $PDO->query("SELECT * FROM usuarios WHERE nome='$nome' OR email='$nome' AND senha='$senha' ");
        
        if($selecionar_user->rowCount() > 0 ){

            foreach($selecionar_user->fetchAll() as $dadosM){
                $PublicarNOME = $dadosM['nome'];
            }

            session_start();
            $_SESSION["Nome"] = $PublicarNOME;
            $PDO->query("UPDATE usuarios SET logoff='$exibirdata' WHERE nome='$nome' ");
            echo "<script> location.href='http://localhost/PassandoAqui/' </script>";

        }
        else {
            echo "<script> alert('Usuário ou senha incorreto!'); </script>";
        }

    }
        

    if(isset($_GET["funcao"]) AND $_GET["funcao"] == "gostei"){

        $meuidgostei = $_GET["id"];

        $SelecionarGostei = $PDO->query("SELECT * FROM posters WHERE ID='$meuidgostei'");

        if($SelecionarGostei>0){

            $SelecionarAvalia = $PDO->query("SELECT * FROM avalia_posters WHERE id_posters='$meuidgostei$NickUser' ");

            if($SelecionarAvalia->rowCount() == 0){
                $PDO->query("INSERT INTO avalia_posters (id_posters, nome_gostei) VALUES ('$meuidgostei$NickUser','$NickUser') ");
                $gosteiAdd = 1;
                $naogosteiAdd = 0;
            }
            else{

                $VerificaAvalia = $PDO->query("SELECT * FROM avalia_posters WHERE nome_gostei='$NickUser' AND nome_naogostei='' AND id_posters='$meuidgostei$NickUser' ");

                if($VerificaAvalia->rowCount() == 0){
                    $PDO->query("UPDATE avalia_posters SET nome_gostei='$NickUser', nome_naogostei='' WHERE id_posters='$meuidgostei$NickUser' ");
                    $gosteiAdd = 1;
                    $naogosteiAdd = -1;
                }
                else{
                    $PDO->query("DELETE FROM avalia_posters WHERE id_posters='$meuidgostei$NickUser' ");
                    $gosteiAdd = -1;
                    $naogosteiAdd = 0;
                }

            }

            foreach($SelecionarGostei->fetchAll() as $pegarPoster){
                $GosteiAtual = $pegarPoster['gostei'] += $gosteiAdd;
                $inserirGostei = $PDO->query("UPDATE posters SET gostei='$GosteiAtual' WHERE id='$meuidgostei'");

                $NaoGosteiAtual = $pegarPoster['naogostei'] += $naogosteiAdd;
                $inserirNaoGostei = $PDO->query("UPDATE posters SET naogostei='$NaoGosteiAtual' WHERE id='$meuidgostei'");
            }

            echo "<script> location.href='topicos'; </script>"; 

        }

    }

    if(isset($_GET["funcao"]) AND $_GET["funcao"] == "naogostei"){

        $meuidnaogostei = $_GET["id"];

        $SelecionarNaoGostei = $PDO->query("SELECT * FROM posters WHERE ID='$meuidnaogostei'"); 

        if($SelecionarNaoGostei>0){

            $SelecionarAvalia = $PDO->query("SELECT * FROM avalia_posters WHERE id_posters='$meuidnaogostei$NickUser' ");

            if($SelecionarAvalia->rowCount() == 0){
                $PDO->query("INSERT INTO avalia_posters (id_posters, nome_naogostei) VALUES ('$meuidnaogostei$NickUser','$NickUser') ");
                $gosteiAdd = 0;
                $naogosteiAdd = 1;
            }
            else{

                $VerificaAvalia = $PDO->query("SELECT * FROM avalia_posters WHERE nome_gostei='' AND nome_naogostei='$NickUser' AND id_posters='$meuidnaogostei$NickUser' ");

                if($VerificaAvalia->rowCount() == 0){
                    $PDO->query("UPDATE avalia_posters SET nome_gostei='', nome_naogostei='$NickUser' WHERE id_posters='$meuidnaogostei$NickUser' ");
                    $gosteiAdd = -1;
                    $naogosteiAdd = 1;
                }
                else{
                    $PDO->query("DELETE FROM avalia_posters WHERE id_posters='$meuidnaogostei$NickUser' ");
                    $gosteiAdd = 0;
                    $naogosteiAdd = -1;
                }

            }

            foreach($SelecionarNaoGostei->fetchAll() as $pegarPoster){
                $GosteiAtual = $pegarPoster['gostei'] += $gosteiAdd;
                $inserirGostei = $PDO->query("UPDATE posters SET gostei='$GosteiAtual' WHERE id='$meuidnaogostei'");

                $NaoGosteiAtual = $pegarPoster['naogostei'] += $naogosteiAdd;
                $inserirNaoGostei = $PDO->query("UPDATE posters SET naogostei='$NaoGosteiAtual' WHERE id='$meuidnaogostei'");
            }

            echo "<script> location.href='topicos'; </script>"; 

        }

    }

?>