<!DOCTYPE html>
<html lang="pt">
    
<head>
	<title>POLÍTIA DE PRIVACIDADE | YouMeme - BRHUEHUE</title>

    <meta name="keywords" content="Memes, menes, trocadilhos, piadas, frases, you, meme, youmeme, site de memes, criar meme, BR">
    <meta name="description" content="Crie uma conta ou entre no YouMeme. Use sua criatividade para criar e compartilhar seus memes com seus amigos. Compartilhe fotos e vídeos!" >
    
	<meta name="viewport" content="width-width=device, initial-scale=1.0">
    
	<link rel="stylesheet" type="text/css" href="css/estilo-padrao.css">
    <link rel="stylesheet" type="text/css" href="css/estilo-formularios.css">
    <link rel="stylesheet" type="text/css" href="css/estilo-mobile.css">
    <link rel="shortcut icon" href="imagens/Icon_Site.png" type="image/x-icon" >
    
</head>

<body>

    <section id="cabecalho">
        <a href="./">
            <img src="imagens/logo.png">
            <p>Deixe um recado ou meme criativo!</p>
        </a>
    </section> 
    
    <!-- TERMOS - BOX -->
    <section id="box-info">
        
    <h1 align="center" >POLÍTICA DE PRIVACIDADE  | YOU MEME</h1><br><hr><br>
        
    <p><b>SEÇÃO 1 - O QUE FAREMOS COM A INFORMAÇÃO?</b><br>
    Quando você realiza alguma transação com nosso site, como parte do processo de cadastro, coletamos as informações pessoais que você nos dá tais como: nome, data de nascimento, e-mail.<br>
    Quando você acessa nosso site, também recebemos automaticamente o protocolo de internet do seu computador, endereço de IP, a fim de obter informações que nos ajudam a aprender sobre seu navegador e sistema operacional.<br><br>
        
    <b>SEÇÃO 2 - DIVULGAÇÃO</b><br>
    Podemos divulgar suas informações pessoais caso sejamos obrigados pela lei para fazê-lo ou se você violar nossos Termos de Serviço.<br><br>
    
    <b>SEÇÃO 3 - SERVIÇOS DE TERCEIROS</b><br>
    No geral, os fornecedores terceirizados usados por nós irão apenas coletar, usar e divulgar suas informações na medida do necessário para permitir que eles realizem os serviços que eles nos fornecem.
    Entretanto, certos fornecedores de serviços terceirizados, têm suas próprias políticas de privacidade com respeito à informação que somos obrigados a fornecer para eles.<br>
    Para esses fornecedores, recomendamos que você leia suas políticas de privacidade para que você possa entender a maneira na qual suas informações pessoais serão usadas por esses fornecedores.<br>
    Em particular, lembre-se que certos fornecedores podem ser localizados em ou possuir instalações que são localizadas em jurisdições diferentes que você ou nós. Assim, se você quer continuar com uma transação que envolve os serviços de um fornecedor de serviço terceirizado, então suas informações podem tornar-se sujeitas às leis da(s) jurisdição(ões) nas quais o fornecedor de serviço ou suas instalações estão localizados.<br>
    Uma vez que você deixe o site redirecionado para um aplicativo ou site de terceiros, você não será mais regido por essa Política de Privacidade ou pelos Termos de Serviço do nosso site.<br><br>
    <b>Links:</b>
    Quando você clica em links no nosso site, eles podem lhe direcionar para fora do nosso site. Não somos responsáveis pelas práticas de privacidade de outros sites e lhe incentivamos a ler as declarações de privacidade deles.<br><br>
        
    <b>SEÇÃO 4 - SEGURANÇA</b><br>
    Para proteger suas informações pessoais, tomamos precauções razoáveis e seguimos as melhores práticas da indústria para nos certificar que elas não serão perdidas inadequadamente, usurpadas, acessadas, divulgadas, alteradas ou destruídas.<br>
    Se você nos fornecer as suas informações, essa informação é criptografada. Embora nenhum método de transmissão pela Internet ou armazenamento eletrônico é 100% seguro.<br><br>
        
    <b>SEÇÃO 5 - ALTERAÇÕES PARA ESSA POLÍTICA DE PRIVACIDADE</b><br>
    Reservamos o direito de modificar essa política de privacidade a qualquer momento, então por favor, revise-a com frequência. Alterações e esclarecimentos vão surtir efeito imediatamente após sua publicação no site. Se fizermos alterações de materiais para essa política, iremos notificá-lo aqui que eles foram atualizados, para que você tenha ciência sobre quais informações coletamos, como as usamos, e sob que circunstâncias, se alguma, usamos e/ou divulgamos elas.
        
    <br><br>Atenciosamente,<br>Equipe YouMeme.</p>
    </section>
    
    <footer>
        <p><a href="https://www.instagram.com/jeysson_mourasz/" target="_blank" >CEO</a> | <a href="https://www.instagram.com/marcosnemrode/" target="_blank" >COO</a> | <a href="termos-de-servico"  >TERMOS</a> | <a href="politica-de-privacidade" >PRIVACIDADE</a> YouMeme © 2017</p>
    </footer>
    
</body>
 
</html>