<?php

    require_once('../conexao.php');

    $Nome_Delete = $_GET['nome'];

    $posters = $PDO->query("SELECT * FROM posters WHERE nick='$Nome_Delete' ");

    foreach($posters->fetchAll() as $exibir){

        $PublicarIMG = $exibir['img'];
        $PublicarVIDEO = $exibir['video'];

        $NomeImg = substr(strrchr($PublicarIMG, "/"), 1);
        $ImgDelete = "../upload/". $NomeImg;

        $NomeVideo = substr(strrchr($PublicarVIDEO, "/"), 1);
        $VideoDelete = "../upload/". $NomeVideo;
        
    }

    if(file_exists($ImgDelete)){
        unlink($ImgDelete);
    }

    if(file_exists($VideoDelete)){
        unlink($VideoDelete);
    }

    $Perfil = $PDO->query("SELECT * FROM usuarios WHERE nome='$Nome_Delete' ");

    foreach($Perfil->fetchAll() as $exibir_perfil_img){

        $PerfilIMG = $exibir_perfil_img['perfil'];
        
        $NomePerfil = substr(strrchr($PerfilIMG, "/"), 1);
        $PerfilDelete = "../upload/". $NomePerfil;
    
    }

    if(file_exists($PerfilDelete)){
        unlink($PerfilDelete);
    }

    if($Nome_Delete != ""){
        $deletar_usuario = $PDO->query("DELETE FROM usuarios WHERE nome='$Nome_Delete' ");
        $deletar_recados = $PDO->query("DELETE FROM recados WHERE autor='$Nome_Delete' ");
        $deletar_seguidores = $PDO->query("DELETE FROM seguidores WHERE nickseguir='$Nome_Delete' OR seguindo='$Nome_Delete' ");
        $deletar_posters = $PDO->query("DELETE FROM posters WHERE nick='$Nome_Delete' ");
        $deletar_comentarios = $PDO->query("DELETE FROM comentarios WHERE nick_comentario='$Nome_Delete' ");
        $deletar_avalia_posters = $PDO->query("DELETE FROM avalia_posters WHERE id_posters LIKE '%$Nome_Delete%' ");
    }

    if($deletar_usuario && $deletar_recados && $deletar_seguidores && $deletar_posters && $deletar_comentarios && $deletar_avalia_posters){
        echo "<script> location.href='../login?func=sairpagina';  alert('Conta deletada com sucesso!'); </script>";
    }
    else{
        echo "<script> alert('Erro ao tentar excluir!'); </script>"; 
    }