<?php
    require_once('../conexao.php');

    $ID_Delete = $_GET['id'];

    $posters = $PDO->query("SELECT * FROM posters");

    foreach($posters->fetchAll() as $exibir){

        $PublicarIMG = $exibir['img'];
        $PublicarVIDEO = $exibir['video'];

        $NomeImg = substr(strrchr($PublicarIMG, "/"), 1);
        $ImgDelete = "../upload/". $NomeImg;

        $NomeVideo = substr(strrchr($PublicarVIDEO, "/"), 1);
        $VideoDelete = "../upload/". $NomeVideo;
        
    }

    if(file_exists($ImgDelete)){
        unlink($ImgDelete);
    }

    if(file_exists($VideoDelete)){
        unlink($VideoDelete);
    }

    $deletar = $PDO->query("DELETE FROM posters WHERE id=$ID_Delete ");
    $deletar_comentarios = $PDO->query("DELETE FROM comentarios WHERE id_post=$ID_Delete ");

    if($deletar && $deletar_comentarios){
        echo "<script> location.href='./'; </script>";
    }
    else{
        echo "<script> alert('Erro ao tentar excluir!'); location.href='./'; </script>"; 
    }