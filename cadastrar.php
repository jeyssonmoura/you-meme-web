<?php

    require_once ('conexao.php');

    setlocale( LC_ALL, 'pt_BR', 'pt_BR.iso-8859-1', 'pt_BR.utf-8', 'portuguese' ); 
    date_default_timezone_set( 'America/Sao_Paulo' );
    $exibirdata = date('Y-m-d');
    $exibirhora = date('H:i:s');
    $exibirano = date('y');

?>

<!DOCTYPE html>
<html lang="pt">
    
<head>
    
	<title>YouMeme - CADASTRO</title>

    <meta name="keywords" content="Memes, menes, trocadilhos, piadas, frases, you, meme, youmeme, site de memes, criar meme, BR">
    <meta name="description" content="Crie uma conta ou entre no YouMeme. Use sua criatividade para criar e compartilhar seus memes com seus amigos. Compartilhe fotos e vídeos!" >
	<meta name="viewport" content="width-width=device, initial-scale=1.0">
    
	<link rel="stylesheet" type="text/css" href="css/estilo-padrao.css">
    <link rel="stylesheet" type="text/css" href="css/estilo-formularios.css">
    <link rel="stylesheet" type="text/css" href="css/estilo-mobile.css">
    
    <link rel="shortcut icon" href="imagens/Icon_Site.png" type="image/x-icon" >
    
    <script async src="js/jquery-3.2.1.min.js"></script>
    <script async src="js/funcoes-site.js"></script>
    
</head>

<body>
    
    <script>
        
        window.onload = Ano;

        function Ano() {
            document.getElementById('ano').value = '1999';
        }
        
    </script>
    
    <!-- SELO -->
    <section class="selo">
        <a href="https://www.facebook.com/youmemeBR/" target="_blank" ><img src="imagens/Selo.png"></a>
    </section>
    
    <!-- CADASTRO - BOX -->
    <section class="box-cadastro">
        <a href="./"><img src="imagens/logo.png"></a>
        <h1>SEJA UM MEMBRO!</h1>
        <form action="cadastrar.php" method="POST">
            <input type="text" name="usuario" maxlength="25" placeholder="Nick ou Nome" required >
            <input type="text" name="email" placeholder="E-mail" required >
            <input type="password" name="senha" placeholder="Senha" required ><br><br>
            <label>Data de nascimento</label><br>
            <select class="select-style" name="dia">
                <?php for($i = 1; $i<32; $i++){ ?>
                <option value="<?php echo $i ?>"><?php echo $i ?></option>
                <?php } ?>
            </select>
            <select name="mes">
                <option value="0">Mês</option>
                <option value="01">Janeiro</option>
                <option value="02">Fevereiro</option>
                <option value="03">Março</option>
                <option value="04">Abril</option>
                <option value="05">Maio</option>
                <option value="06">Junho</option>
                <option value="07">Julho</option>
                <option value="08">Agosto</option>
                <option value="09">Setembro</option>
                <option value="10">Outubro</option>
                <option value="11">Novembro</option>
                <option value="12">Dezembro</option>
            </select>
            <select id="ano" name="ano">
                <?php for($a = 1905; $a<$exibirano+2001; $a++){ ?>
                <option value="<?php echo $a ?>"><?php echo $a ?></option>
                <?php } ?>
            </select><br><br>
            <input type="radio" name="sexo" value="1" required ><label>MASCULINO</label>
            <input type="radio" name="sexo" value="0" required ><label>FEMININO</label>
            <input class="botao-postar" type="submit" name="bt-enviar-cadastro" value="PARTICIPAR!"><br><br>
        </form>
    </section>
    
    <section class="box-mensagem">
        <p>Ao clicar em PARTICIPAR!, você concorda com os<br>
        <a href="termos-de-servico" target="_blank" >Termos de Serviço</a> e a <a href="politica-de-privacidade" target="_blank" >Política de Privacidade</a></p>
    </section>
    
    <footer>
        <p><a href="https://www.instagram.com/jeysson_mourasz/" target="_blank" >CEO</a> | <a href="https://www.instagram.com/marcosnemrode/" target="_blank" >COO</a> | <a href="termos-de-servico" target="_blank" >TERMOS</a> | <a href="politica-de-privacidade" target="_blank" >PRIVACIDADE</a> YouMeme © 2017</p>
    </footer>
    
</body>

</html>

<?php

    if(isset($_POST['bt-enviar-cadastro'])){
        
        $nome = addslashes($_POST['usuario']);
        $email = addslashes($_POST['email']);
        $senha = md5($_POST['senha']);
        $sexo = $_POST['sexo'];
        $dia = $_POST['dia'];
        $mes = $_POST['mes'];
        $ano = $_POST['ano'];
        $codimg = rand(0000, 9999);
        $dataregistro = date('Y-m-d');
         
        if(filter_var($email, FILTER_VALIDATE_EMAIL)){}else{
            echo "<script> location.href='cadastrar'; alert('Este Email não é valido!'); </script>";
            return true;
        }
        
        if($mes != 0){
            
            $inserir_user = $PDO->query("SELECT * FROM usuarios WHERE email='$email' OR nome='$nome'");

            if($inserir_user->rowCount() == 0){
                
                $datanascimento = $ano.'-'.$mes.'-'.$dia;
                
                $insertuser = $PDO->prepare("INSERT INTO usuarios (nome, email, senha, sexo, idade, registrado, CODIMG)"."VALUES (?,?,?,?,?,?,?)");
                $insertuser->bindParam(1, $nome);
                $insertuser->bindParam(2, $email);
                $insertuser->bindParam(3, $senha);
                $insertuser->bindParam(4, $sexo);
                $insertuser->bindParam(5, $datanascimento);
                $insertuser->bindParam(6, $dataregistro);
                $insertuser->bindParam(7, $codimg);

                $insertuser->execute();

                if($insertuser > 0) {
                    echo "<script> location.href='login'; alert('Conta criada com sucesso!'); </script>";
                }else {
                    echo "<script> location.href='cadastrar'; alert('Falha ao realizar cadastro!'); </script>";
                }
                
            }
            else {
                echo "<script> location.href='cadastrar'; alert('Esta conta já existe!'); </script>";
            }
            
        }
        else {
            echo "<script> location.href='cadastrar'; alert('Preencha todos os campos!'); </script>";
        }  
        
    }

?>