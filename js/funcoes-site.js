/* ------------ EXCLUIR ------------ */
function excluir(id) {
    if(confirm("Deseja excluir esta publicação?")){
        window.location.href = "funcoes/excluir.php?id="+id;
    }
}

/* ------------ EXCLUIR - RECADO ------------ */
function excluirRecado(id) {
    if(confirm("Deseja excluir este recado?")){
        window.location.href = "funcoes/excluir_recado.php?id="+id;
    }
}

/* ------------ EXCLUIR - COMENTARIO ------------ */
function excluirComentario(id) {
    if(confirm("Deseja excluir este comentário?")){
        window.location.href = "funcoes/excluir_comentario.php?id="+id;
    }
}

/* ------------ EXCLUIR - Conta ------------ */
function excluirConta(nome) {
    if(confirm("Deseja excluir esta conta?")){
        window.location.href = "funcoes/excluir_conta.php?nome="+nome;
    }
}

/* ------------ TAGS - MASCARA ------------ */
function mascTAG (texto){
	if (event.keyCode !== 8){
		if (texto.length === 1) {
			document.getElementById('tags').value = "#" + texto;
		}
	}
}

/* ------------ POPUP ------------ */
function fechar_popup() {
    document.getElementById('box-popup-fundo').style.display = 'none';
}
function abrir_popup() {
    document.getElementById('box-popup-fundo').style.display = 'block';
}

/* ------------ AVISOS ------------ */
function fechar_avisos() {
    document.getElementById('box-avisos').style.display = 'none';
}

/* ------------ NOTIFICACOES ------------ */
function fechar_notificacoes() {
    document.getElementById('popup-notificacoes').style.display = 'none';
    document.getElementById('bt-notificacoes-a').style.display = 'block';
    document.getElementById('bt-notificacoes-f').style.display = 'none';
}

function abrir_notificacoes() {
    document.getElementById('popup-notificacoes').style.display = 'block';
    document.getElementById('bt-notificacoes-a').style.display = 'none';
    document.getElementById('bt-notificacoes-f').style.display = 'block';
}

/* ------------ RECUPERAR SENHA ------------ */
function fechar_recuperar() {
    document.getElementById('recuperar-senha-box1').style.display = 'none';
    document.getElementById('recuperar-senha-box2').style.display = 'block';
}

function abrir_recuperar() {
    document.getElementById('recuperar-senha-box1').style.display = 'none';
    document.getElementById('recuperar-senha-box2').style.display = 'block';
}

/* ------------ POPUP - EMOJIS ------------ */
function fechar_emojis() {
    document.getElementById('bt-abrir-emojis').style.display = 'block';
    document.getElementById('bt-fechar-emojis').style.display = 'none';
    document.getElementById('box-emojis-select').style.display = 'none';
}

function abrir_emojis() {
    document.getElementById('bt-abrir-emojis').style.display = 'none';
    document.getElementById('bt-fechar-emojis').style.display = 'block';
    document.getElementById('box-emojis-select').style.display = 'block';
}

/* ------------ EMOJIS ------------ */
function addEmoji(emoji_id) {
    document.getElementById("box-frase").value += ' [E'+emoji_id+'] ';
}
        
/* ------------ ALTERAR ------------ */
function edit_ativado() {
    document.getElementById('alterar-perfil-text').style.display = 'block';
    document.getElementById('alterar-perfil-nick').style.display = 'none';
    document.getElementById('alterar-perfil-bt').style.display = 'block';
    document.getElementById('box-perfil').style.minHeight = '480px';
}

function edit_desativado() {
    document.getElementById('alterar-perfil-bt').style.display = 'none';
    document.getElementById('alterar-perfil-text').style.display = 'none';
    document.getElementById('alterar-perfil-nick').style.display = 'block';
    document.getElementById('box-perfil').style.minHeight = '135px';
}

/* ------------ BOTAO IMAGEM ------------ */
function enviar_img() {
    document.getElementById('botao-img-perfil').style.display = 'block';
}

/* ------------ TOPICOS - BOX ------------ */
function fechar_topicos() {
    document.getElementById('box-mais-tocipos').style.display = 'none';
    document.getElementById('box-menos-t').style.display = 'none';
    document.getElementById('box-mais-t').style.display = 'block';
    document.getElementById('topicos-box').style.minHeight = '110px';
}

function abrir_topicos() {
    document.getElementById('box-mais-tocipos').style.display = 'block';
    document.getElementById('box-menos-t').style.display = 'block';
    document.getElementById('box-mais-t').style.display = 'none';
    document.getElementById('topicos-box').style.minHeight = '140px';
}

/* ------------ AJAX ------------ */


/*
$("#botao").click(function() {
    $("#mensagem").append('<img class="emoji-img" width="25" height="25" src="imagens/passandoaqui.gif">');
});
function PegarMens(){
    document.getElementById("box-frase").value = document.getElementById("mensagem").innerHTML;
}
*/