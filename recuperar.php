<?php

    require_once('conexao.php');

?>
<!DOCTYPE html>
<html lang="pt">
    
<head>
	<title>YouMeme - RECUPERAR</title>

    <meta name="keywords" content="Memes, menes, trocadilhos, piadas, frases, you, meme, youmeme, site de memes, criar meme, BR">
    <meta name="description" content="Crie uma conta ou entre no YouMeme. Use sua criatividade para criar e compartilhar seus memes com seus amigos. Compartilhe fotos e vídeos!" >
    
	<meta name="viewport" content="width-width=device, initial-scale=1.0">
    
	<link rel="stylesheet" type="text/css" href="css/estilo-padrao.css">
    <link rel="stylesheet" type="text/css" href="css/estilo-formularios.css">
    <link rel="stylesheet" type="text/css" href="css/estilo-mobile.css">
    <link rel="shortcut icon" href="imagens/Icon_Site.png" type="image/x-icon" >
    
    <script async src="js/jquery-3.2.1.min.js"></script>
    <script async src="js/funcoes-site.js"></script>
    
</head>

<body>

    <section id="cabecalho">
        <a href="login">
            <img src="imagens/logo.png">
            <p>Deixe um recado ou meme criativo!</p>
        </a>
    </section> 
    
    <style>
        
        #recuperar-senha-box2 {
            display: none;
        }
        
    </style>
    
    <!-- SELO -->
    <section class="selo">
        <a href="https://www.facebook.com/youmemeBR/" target="_blank" ><img src="imagens/Selo.png"></a>
    </section>

    <!-- GERAL - BOX -->
    <section id="box-geral">
    
        <!-- RECUPERAR - BOX - CODIGO -->
        <section id="recuperar-senha-box1" class="box-design" >
            <h1>Encontre sua conta!</h1>
            <form method="POST">
                <input type="text" name="email" placeholder="E-mail" required >
                <input class="botao-postar" type="submit" name="bt-cod" value="ENVIAR CÓDIGO"><br><br>
            </form>
        </section>
        
        <!-- RECUPERAR - BOX - NOVA SENHA -->
        <section id="recuperar-senha-box2" class="box-design" >
            <form method="POST">
                <input type="text" name="codigo" placeholder="Código" required ><br>
                <input type="password" name="senha" placeholder="Nova senha" required ><br>
                <input type="password" name="senha-c" placeholder="Confirmar nova senha" required ><br>
                <input class="botao-postar" type="submit" name="bt-recuperar" value="ALTERAR SENHA"><br><br>
            </form>
        </section>

    </section>
        
    <section class="box-mensagem">
        <h1>Um código será enviado para o seu e-mail cadastrado,<br> ao recebe-lo copie e cole no campo requerido acima, <br>
        em seguida digite a nova senha.</h1>
    </section>

    <footer>
        <p><a href="https://www.instagram.com/jeysson_mourasz/" target="_blank" >CEO</a> | <a href="https://www.instagram.com/marcosnemrode/" target="_blank" >COO</a> | <a href="termos-de-servico" target="_blank" >TERMOS</a> | <a href="politica-de-privacidade" target="_blank" >PRIVACIDADE</a> YouMeme © 2017</p>
    </footer>
    
</body>
 
</html>


<?php

    if(isset($_POST['bt-cod'])){
        
        $email = $_POST['email'];
        $codsenha = rand(0000000, 9999999);
             
        if(filter_var($email, FILTER_VALIDATE_EMAIL)){

            $mandar_cod = $PDO->query("UPDATE usuarios SET codsenha='$codsenha' WHERE email='$email' ");

            if($mandar_cod->rowCount() > 0){
                
                $assunto = "YOU MEME | Recuperação de senha";

                $meuEmail = "
                Suporte - YOU MEME!

                Recuperação de senha para o E-mail: $email

                SEU CÓDIGO:
                $codsenha";

                $enviarEmail = mail($email, $assunto, $meuEmail);
                
                echo "<script> location.href='javascript:abrir_recuperar();' </script>";
            }
            else {
                echo "<script> alert('Usuário não existe!') </script>";
            }
            
            
        }else{
            echo "<script> alert('Este Email não é valido!') </script>";
            return true;
        }
        
    }

    if(isset($_POST['bt-recuperar'])){
        
        $codsenhac = $_POST['codigo'];
        $senha = $_POST['senha'];
        $senhac = $_POST['senha-c'];
        
        if($senha == $senhac){
            
            $inserir_nova_senha = $PDO->query("UPDATE usuarios SET senha='".md5($senha)."' WHERE codsenha='$codsenhac' ");

            if($inserir_nova_senha->rowCount() > 0){

                echo "<script> alert('Senha alterada com sucesso!'); location.href='login.php'; </script>"; 
                
            }
            else {
                echo "<script> alert('Usuário não existe!'); </script>";
            }
            
        }
        else{
            echo "<script> alert('As senhas não são iguais!'); </script>"; 
        }
            
    }

?>