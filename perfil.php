<?php

    require_once('conexao.php');
    session_start();

    include("proteger-user.php");
    protegerpagina();
    sairpagina();

    $NickUser = ucwords($_SESSION["Nome"]);
    $NickVisitante = ucwords($_GET['nick']);

    $dadosUser = $PDO->query("SELECT * FROM usuarios WHERE nome='$NickUser' ");

    if($dadosUser->rowCount() > 0 ){

        foreach($dadosUser->fetchAll() as $user_dados){
            $PublicarSEXO_USER = $user_dados['sexo'];
            $PublicarIDADE_USER = $user_dados['idade'];
            $PublicarPERFIL_USER = $user_dados['perfil'];
            $PublicarNOME_USER = $user_dados['nome'];
            $PublicarADM_USER = $user_dados['ADM'];
            $PublicarID_USER = $user_dados['id'];
            $PublicarNOTIFICAR_USER = $user_dados['notificar'];
            $codimg = $user_dados['CODIMG'];
        }

    }

    $dadosVisitante = $PDO->query("SELECT * FROM usuarios WHERE nome='$NickVisitante' ");

    if($dadosVisitante->rowCount() > 0 ){

        foreach($dadosVisitante->fetchAll() as $Visitante_dados){
            $PublicarSEXO_Visitante = $Visitante_dados['sexo'];
            $PublicarIDADE_Visitante = $Visitante_dados['idade'];
            $PublicarPERFIL_Visitante = $Visitante_dados['perfil'];
            $PublicarADM_Visitante = $Visitante_dados['ADM'];
            $PublicarNOME_Visitante = $Visitante_dados['nome'];
        }

    }

    $notifica = $PDO->query("SELECT * FROM posters ORDER BY id DESC LIMIT 0, 10");
    
    setlocale( LC_ALL, 'pt_BR', 'pt_BR.iso-8859-1', 'pt_BR.utf-8', 'portuguese' ); 
    date_default_timezone_set( 'America/Sao_Paulo' );
    $exibirdata = date('Y-m-d');
    $exibirhora = date('H:i:s');

?>

<!DOCTYPE html>
<html lang="pt">
    
<head>
    
	<title><?php if($PublicarNOTIFICAR_USER>0){ echo "($PublicarNOTIFICAR_USER)"; } ?> PERFIL - 
        <?php if(!$_GET['nick']){
            echo $NickUser;
        }
        else{
            echo $NickVisitante;
        } ?>
    </title>
    
    <meta name="keywords" content="Memes, menes, trocadilhos, piadas, frases, you, meme, youmeme, site de memes, criar meme, BR">
    <meta name="description" content="Crie uma conta ou entre no YouMeme. Use sua criatividade para criar e compartilhar seus memes com seus amigos. Compartilhe fotos e vídeos!" >
	<meta name="viewport" content="width-width=device, initial-scale=1.0">
    
    <link rel="stylesheet" type="text/css" href="css/estilo-padrao.css">
    <link rel="stylesheet" type="text/css" href="css/estilo-mobile.css">
    <link rel="stylesheet" type="text/css" href="css/estilo-perfil.css">
    <link rel="stylesheet" type="text/css" href="css/estilo-formularios.css">
    <link rel="stylesheet" type="text/css" href="css/estilo-notificacoes.css">
    <link rel="shortcut icon" href="imagens/Icon_Site.png" type="image/x-icon" >
    
    <script async src="js/jquery-3.2.1.min.js"></script>
    <script async src="js/funcoes-site.js"></script>
    
</head>

<body>
    
    <!-- SELO -->
    <section class="selo">
        <a href="https://www.facebook.com/youmemeBR/" target="_blank" ><img src="imagens/Selo.png"></a>
    </section>
    
    <!-- PERFIl - MENU -->
    <?php if($NickUser != ""){ ?>
    <nav class="menu">
        <ul>
            <a href="<?php echo $NickUser ?>"><img class="pefil-img" src="<?php echo $PublicarPERFIL_USER ?>"></a>
            <li><a href="<?php echo $NickUser ?>"><strong class="meu-nick"><?php echo $NickUser ?></strong></a></li>
            <li><form method="get" ><input type="search" name="buscar" placeholder="Buscar..."></form></form></li>

            <figure class="pefil-notificacao">

                <section id="bt-notificacoes-a">
                    <a href="javascript:abrir_notificacoes();"><img class="pefil-icons" src="imagens/icon-notificacao.png"></a>
                </section>

                <section id="bt-notificacoes-f">
                    <a onClick="javascript:fechar_notificacoes();" 
                       href="<?php if($PublicarNOTIFICAR_USER > 0){ echo "?notifique=Notificar"; } else { echo "#"; } ?> "> 
                       <img class="pefil-icons" src="imagens/icon-notificacao.png"></a>
                </section>

                <figcaption> 
                    <p><?php if($PublicarNOTIFICAR_USER>0){ echo $PublicarNOTIFICAR_USER; } ?></p>
                </figcaption>
            </figure>
    

            <!-- NOTIFICAÇÕES - BOX -->
            <section id="popup-notificacoes">
                <h1>NOTIFICAÇÕES <?php if($PublicarNOTIFICAR_USER>0){ echo "($PublicarNOTIFICAR_USER)"; } ?></h1>
                <section class="barra-notificacoes">
                    <?php

                    if(isset($_GET["notifique"]) AND $_GET["notifique"] == "Notificar" && $PublicarNOTIFICAR_USER > 0 ){
                        $VerificarNotificar = 0;
                        $PDO->query("UPDATE usuarios SET notificar='$VerificarNotificar' WHERE nome='$NickUser' ");
                        echo "<script> location.href='index.php'; </script>";
                    }

                    if($notifica->rowCount() > 0 ){

                        foreach($notifica->fetchAll() as $exibir_notifica){
                            
                            $PublicarID_NOTIFICA = $exibir_notifica['id'];
                            $PublicarIMG_NOTIFICA = $exibir_notifica['img'];
        
                    ?>
                    <p><img src="<?php if($PublicarIMG_NOTIFICA!=""){echo $PublicarIMG_NOTIFICA;} else {echo "imagens/sem-foto.png"; } ?>"><a href="poster.php?id=<?php echo $PublicarID_NOTIFICA ?>">Passando aqui para:<br>NOVO MEME!</a></p>
                    <?php } } else { ?>
                    <h1>Nenhuma notificação...</h1>
                    <?php } ?>
                </section>
            </section>
            <li>
                <a href="#"><img src="imagens/icon-eng.png"></a>
                <ul>
                    <li><a href="<?php echo $NickUser ?>">PERFIL</a></li>
                    <li><a href="?func=sairpagina">SAIR</a></li>
                </ul>
            </li>
        </ul>
    </nav><br><br>
    <?php } ?>
    
    <section id="cabecalho">
        <a href="./">
            <img src="imagens/logo.png">
            <p>Deixe um recado ou meme criativo!</p>
        </a>
    </section>  
            
<?php if($dadosVisitante->rowCount() > 0 || $NickUser == $NickVisitante){ ?>

    <!-- PERFIl - BOX -->
    <section id="box-perfil">
        
            <figure class="pefil-miniatura">
                
                <?php if($NickUser == "" && $NickVisitante == ""){ ?>
                <img class="pefil-minuatura-img" src="<?php echo $PublicarPERFIL_USER ?>">
                <?php if($PublicarADM == 1){ ?>
                    <img class="icon-perfil-coroa" src="imagens/icon-admin.png">
                <?php } ?>
                <?php } else { ?>
                <?php if($PublicarADM_Visitante == 1){ ?>
                    <img class="icon-perfil-coroa" src="imagens/icon-admin.png">
                <?php } ?>
                <img class="pefil-minuatura-img" src="<?php echo $PublicarPERFIL_Visitante ?>">
                <?php } ?>

                
                <figcaption>
                    <?php if($NickUser == $NickVisitante){ ?>
                    <form method="post" enctype="multipart/form-data">
                        <label for='img-perfil-enviar'>
                            <a id='nome-arquivo' onClick="javascript:enviar_img();" >ALTERAR</a>
                        </label>
                        <input id='img-perfil-enviar' type='file' accept="image/jpeg" name="img-perfil-enviar">
                        <input id="botao-img-perfil" type="submit" name="bt-enviar-img" value="OK!">
                    </form>
                    <?php } ?>
                </figcaption>
            </figure>
            
            <style>
                #alterar-perfil-text {
                    display: none;
                }
                #alterar-perfil-bt {
                    display: none;
                }
                #alterar-perfil-nick {
                    display: block;
                }
                #box-popup-fundo {
                    display: none;
                }
            </style>
        
            <section id="box-perfil-info">
                <form method="post">
                    
                    <alterar id="alterar-perfil-text" >
                        
                        <a href="javascript:edit_desativado();"><img src="imagens/icon-alterar-fechar.png"></a>
                        
                        <p>Redefinir Nick</p>
                        <input type="text" name="alterarnick" value="<?php echo $NickUser ?>"><br>
                        
                        <script>
                            
                            window.onload = PegarDados;

                            function PegarDados() {
                                document.getElementById('sexo').value = '<?php echo $PublicarSEXO_USER ?>';
                                document.getElementById('dia').value = '<?php $diaAlt = substr($PublicarIDADE_USER, -2); 
                                                                         if($diaAlt < 10) { echo substr($PublicarIDADE_USER, -1); } 
                                                                         else { echo $diaAlt; }  ?>';
                                document.getElementById('mes').value = '<?php echo substr($PublicarIDADE_USER, 5, -3); ?>';
                                document.getElementById('ano').value = '<?php echo substr($PublicarIDADE_USER, -10, 4); ?>';
                            }

                        </script>
                        
                        <p>Redefinir Gênero</p><br><br><br>
                        <select id="sexo" name="sexo">
                            <option value="1">Masculino</option>
                            <option value="0">Feminino</option>
                        </select><br><br>
                        
                        <a class="bt-deletar" href="javascript:func();" onclick="excluirConta('<?php echo $PublicarNOME_USER; ?>')">DELETAR PERFIL</a>
                        <br><br><br>
                        
                        <p>Redefinir Senha</p>
                        <input type="password" name="senha-a" placeholder="Senha Antiga" ><br>
                        <input type="password" name="senha-n" placeholder="Nova senha" ><br>
                        <input type="password" name="senha-c" placeholder="Confirmar nova senha" ><br><br><br><br><br>
                        
                        <p>Data de nascimento</p><br>
                        <select id="dia" name="dia">
                            <?php for($i = 1; $i<32; $i++){ ?>
                            <option value="<?php echo $i ?>"><?php echo $i ?></option>
                            <?php } ?>
                        </select>
                        <select id="mes" name="mes">
                            <option value="0">Mês</option>
                            <option value="01">Janeiro</option>
                            <option value="02">Fevereiro</option>
                            <option value="03">Março</option>
                            <option value="04">Abril</option>
                            <option value="05">Maio</option>
                            <option value="06">Junho</option>
                            <option value="07">Julho</option>
                            <option value="08">Agosto</option>
                            <option value="09">Setembro</option>
                            <option value="10">Outubro</option>
                            <option value="11">Novembro</option>
                            <option value="12">Dezembro</option>
                        </select>
                        <select id="ano" name="ano">
                            <?php for($a = 1905; $a<$exibirano+2001; $a++){ ?>
                            <option value="<?php echo $a ?>"><?php echo $a ?></option>
                            <?php } ?>
                        </select>

                    </alterar>
                    
                    <alterar id="alterar-perfil-nick" >
                        
                        <?php if($NickUser == $NickVisitante){ ?><a href="javascript:edit_ativado();"><img src="imagens/icon-alterar.png"></a><?php } ?>
                        Nick/Nome: <strong><?php echo $NickVisitante; ?></strong><br>

                        Gênero: <?php 

                        if(isset($_SESSION["Nome"]) && !$_GET['nick']){
                            if($PublicarSEXO_USER==0){
                                echo "FEMININO"; 
                            }else{
                                echo "MASCULINO"; 
                            }
                        } else {
                            if($PublicarSEXO_Visitante==0){
                                echo "FEMININO"; 
                            }else{
                                echo "MASCULINO"; 
                            }
                        } ?><br>

                        Idade: <?php if($NickUser == $NickVisitante){
                            echo $exibirdata-$PublicarIDADE_USER;
                        } else{
                            echo $exibirdata-$PublicarIDADE_Visitante;
                        } ?> Anos <br>

                        <?php

                            if($NickUser == ""){
                                $NickSeguir = $NickVisitante;
                            }else {
                                if($NickUser == $NickVisitante){
                                    $NickSeguir = $NickUser;
                                }
                                else{
                                    $NickSeguir = $NickVisitante;
                                }
                            }

                            $total_posts = $PDO->query("SELECT * FROM posters WHERE nick='$NickSeguir' ");
                            $qposts = $total_posts->rowCount();

                            $VerificaMeusSeguidores = $PDO->query("SELECT * FROM seguidores WHERE nickseguir LIKE '$NickSeguir' ");

                        ?>

                    <!-- SEGUIR -->
                    <section class="icon-conquistas" >
                        <a href="#"><?php echo $qposts; ?><br><b>POSTS</b></a>
                    </section>

                    <section class="icon-conquistas" id="click-seguidores">
                        <a href="javascript:abrir_popup();"><?php echo $VerificaMeusSeguidores->rowCount(); ?><br><b>SEGUIDORES</b></a>
                    </section>
                        
                    </alterar>

                    <br><br><alterar id="alterar-perfil-bt" >
                        <input type="submit" name="bt-alterar" value="CONCLUIR" >
                    </alterar>
                    
                </form>
            </section>

    </section>
        
    <!-- BOX - SEGUIDORES -->
    <section id="box-popup-fundo">
        <section class="box-popup-f" onclick="javascript:fechar_popup();">
            <section class="box-popup">
                <h1 align="center">SEGUIDORES</h1><b><a href="javascript:fechar_popup();">X</a></b><hr>
                <section class="barra-seguidores">
                    <?php
                                                                         
                        if($VerificaMeusSeguidores->rowCount() > 0 ){

                        foreach($VerificaMeusSeguidores->fetchAll() as $exibir_seguidores){

                            $PublicarNOME_SEGUIDOR = $exibir_seguidores['seguindo'];
                    ?>
                    <h1>
                    <?php 

                        $IMGPerfilSeguidor = $PDO->query("SELECT * FROM usuarios WHERE nome='$PublicarNOME_SEGUIDOR' ");

                        foreach($IMGPerfilSeguidor->fetchAll() as $exibir_seguidores_img){

                            $PublicarIMG_SEGUIDOR = $exibir_seguidores_img['perfil'];
                            $PublicarIDADE_SEGUIDOR = $exibir_seguidores_img['idade'];
                            
                    ?>
                        <a href="<?php echo $PublicarNOME_SEGUIDOR ?>"><img src="<?php  echo $PublicarIMG_SEGUIDOR; } ?>" ></a>
                        <a class="nick-seguir" href="<?php echo $PublicarNOME_SEGUIDOR ?>"><?php echo $PublicarNOME_SEGUIDOR ?></a>
                    </h1>
                    <?php  } } else { ?>
                    <p>Nenhum seguidor...</p>
                    <?php } ?>
                </section>
            </section>
        </section>
    </section>


<?php 

    if($NickUser != $NickVisitante){

    $VerificaUserSeguir = $PDO->query("SELECT * FROM seguidores WHERE nickseguir='$NickVisitante' AND seguindo='$NickUser' ");
        
?>
    <section id="box-menus-perfil">

        <?php if($VerificaUserSeguir->rowCount() == 0 ){ ?>
        <form method="post">
            <input id="icon-seguir" type="submit" name="bt-seguir" value="SEGUIR">
        </form>
        <?php } else { ?>
        <form method="post">
            <input id="icon-desseguir" type="submit" name="bt-seguir" value="DEIXAR DE SEGUIR">
        </form>
        <?php } ?>
        
        <?php
    
            if(isset($_POST['bt-seguir'])){
                
                if($NickUser != ""){
                    
                    if($VerificaUserSeguir->rowCount() > 0 ){
                        $PDO->query("DELETE FROM seguidores WHERE nickseguir='$NickVisitante' ");
                        echo "<script> location.href='$NickVisitante'; </script>"; 
                    }
                    else{
                        $PDO->query("INSERT INTO seguidores (nickseguir, seguindo) VALUES ('$NickVisitante','$NickUser') ");
                        echo "<script> location.href='$NickVisitante'; </script>"; 
                    }
                    
                } else {
                    echo "<script> location.href='cadastrar'; </script>"; 
                }

            }
               
        ?>
        
    </section>

    <?php if($NickUser != "" && $NickUser != $NickVisitante){ ?>

    <!-- AVISOS - BOX -->
    <?php $avisos_e = $_GET['aviso-e']; if($avisos_e != ""){ ?>
    <section id="box-avisos" class="cor-erro">
        <h1 class="cor-erro" ><?php echo $avisos_e; ?><h1><a href="javascript:fechar_avisos();">X</a>
    </section>
    <?php } $avisos_s = $_GET['aviso-s']; if($avisos_s != ""){  ?>
    <section id="box-avisos">
        <h1><?php echo $avisos_s; ?><h1><a href="javascript:fechar_avisos();">X</a>
    </section>
    <?php } ?>

    <section id="box-mensagens-perfil">
    
        <b>DEIXE UM RECADO PARA <?php echo strtoupper($NickVisitante); ?>!</b>
        <form method="post" >
            <textarea name="box-poster-mensagem" contentEditable="true" hidefocus="true" placeholder="Passando aqui para..." ></textarea>
            <input class="botao-evniar-perfil" type="submit" name="bt-enviar-perfil" value="ENVIAR RECADO">
        </form>
        
        <?php 
        
            if(isset($_POST['bt-enviar-perfil'])){
                
                $autor = $NickUser;
                $recado = $_POST['box-poster-mensagem'];
                $remetente = $NickVisitante;

                if($recado != ""){
                    $verifica_enviar = $PDO->query("INSERT INTO recados (autor, recado, data, hora, remetente) VALUES ('$autor', '$recado', '$exibirdata', '$exibirhora', '$remetente' ) ");

                    if($verifica_enviar){
                        echo "<script> location.href='$NickVisitante?aviso-s=Recado enviado com sucesso!'; </script>";
                    }
                    else {
                        echo "<script> location.href='$NickVisitante?aviso-e=Erro ao enviar!'; </script>";
                    }
                }
                else {
                    echo "<script> location.href='$NickVisitante?aviso-e=Preencha todos os campos!'; </script>";
                }
                
            }
            
        ?>

    </section>
<?php } ?>

<?php } else { ?>

<?php if($NickUser != "" && $NickUser == $NickVisitante){ ?>

    <section id="box-mensagens-perfil">
    
        <b>RECADOS RECEBIDOS:</b><br>
        
        <?php 
        
        if(empty($_GET['pg'])){}else {
            $pg = $_GET['pg'];
        }

        if(isset($pg)){
            $pg = $_GET['pg'];
        }
        else {
            $pg = 1;
        }

        $quantidade = 5;
        $inicio = ($pg*$quantidade)- $quantidade;

        $MeusRecados = $PDO->query("SELECT * FROM recados WHERE remetente='$NickUser' ORDER BY id DESC LIMIT $inicio, $quantidade ");
        $total_recados = $PDO->query("SELECT * FROM recados WHERE remetente='$NickUser' ");
        $maxpost = $total_recados->rowCount(); 
        
        if($MeusRecados->rowCount() == 0 ){
        
        ?>
        
        <b>Nenhum recado...</b>
        
        <?php } else {
        
            foreach($MeusRecados->fetchAll() as $exibir_recados){
                $PublicarID_RECADO = $exibir_recados['id'];
                $PublicarAUTOR = $exibir_recados['autor'];
                $PublicarRECADO = $exibir_recados['recado'];
                $PublicarDATA_RECADO = $exibir_recados['data'];
                $PublicarHORA_RECADO = $exibir_recados['hora'];
        ?>
        
        <b><font color="f7ab07"><a href="./<?php echo iconv('UTF-8', 'ASCII//TRANSLIT', $PublicarAUTOR); ?>"><?php echo $PublicarAUTOR; ?></a> - <?php echo $PublicarDATA_RECADO; ?> às <?php echo $PublicarHORA_RECADO; ?></font></b>
        <br><b><?php echo $PublicarRECADO; ?>
        <a class="excluir-recado" href="javascript:func();" onclick="excluirRecado(<?php echo $PublicarID_RECADO; ?>)" ><img src="imagens/icon-excluir.png"></a></b><br><hr>
        <?php } ?>
        
        <!-- PAGINACAO -->
        <section class="link-pag-recados">
        <br><a href="perfil.php?&pg=1">NOVOS</a>  
        <?php

        if($maxpost <= $quantidade){} else {
            
            $links = 5;
            $paginas = ceil($maxpost/$quantidade);
    
            if(isset($i)){} else{
                $i = 1;
            }
    
        }

        if(isset($_GET['pg'])){
            $num_pg = $_GET['pg'];
        }
    
        for($i = $pg-$links; $i <= $pg-1; $i++){
            if($i <= 0){} else { ?>
            <a href="perfil.php?&pg=<?php echo $i; ?>"><?php echo $i; ?></a> 
        <?php } } ?><a href="perfil.php?&pg=<?php echo $i; ?>"><?php echo $i; ?></a>
                
        <?php for($i = $pg+1; $i <= $pg+$links; $i++){
            if($i > $paginas){} else { ?>
                <a href="perfil.php?&pg=<?php echo $i; ?>"><?php echo $i; ?></a>
        <?php } } ?>
        </section>
        <?php } ?>
    </section>

<?php } ?>

<?php } ?>

    <style>
        @media all and (max-width: 480px) {

            #box-perfil-info {
                font: 300 16px Arial, sans-serif;
                width: 60%;
            }

            #box-perfil {
                width: 95%;
            }
            
        }
    </style>

    <!-- JAVASCRIPT IMPORT -->
    <script src='//production-assets.codepen.io/assets/common/stopExecutionOnTimeout-b2a7b3fe212eaa732349046d8416e00a9dec26eb7fd347590fbced3ab38af52e.js'></script><script src='//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

    <script>
        
        /* ------------ IMAGEM/GIF ------------ */
        var $botao = document.getElementById('img-perfil-enviar'),
            $nomearquivo = document.getElementById('nome-arquivo');

        $botao.addEventListener('change', function(){
        $nomearquivo.textContent = this.value;
        });
    
    </script>
    
    <!-- MINHAS POSTAGEM - LISTA - BOX -->
    <?php

    if($NickUser != "" && $NickUser == $NickVisitante){
        $NickPosters = $NickUser;
    }
    else {
        $NickPosters = $NickVisitante;
    }

    $verificar = $PDO->query("SELECT * FROM posters WHERE nick='$NickPosters' ORDER BY id DESC ");

    if($verificar->rowCount() > 0 ){

        foreach($verificar->fetchAll() as $exibir){
            
            $PublicarID = $exibir['id'];
            $PublicarNICK = $exibir['nick']; 
            $PublicarPOSTER = $exibir['poster'];
            $PublicarDATA = $exibir['data'];
            $PublicarHORA = $exibir['hora'];
            $PublicarIMG = $exibir['img'];
            $PublicarVIDEO = $exibir['video'];
            $PublicarGOSTEI = $exibir['gostei'];
            $PublicarNAOGOSTEI = $exibir['naogostei'];
            $PublicarTAG = $exibir['tag'];
            
            $verificar_perfil = $PDO->query("SELECT * FROM usuarios WHERE nome='$PublicarNICK' ");
            
            if($verificar_perfil->rowCount() > 0 ){

            foreach($verificar_perfil->fetchAll() as $perfil_exibir){
                $PublicarPERFIL_VISITANTE = $perfil_exibir['perfil'];
                $PublicarADM_VISITANTE = $perfil_exibir['ADM'];
            ?>
    
            <section id="box-postagens">
                <?php if($PublicarNICK != $NickUser){ ?>
                    <a class="link-perfil" href="<?php echo $PublicarNICK ?>" > 
                        
                        <figure class="admin-icon">
                            <img class="imagem-post" src="<?php echo $PublicarPERFIL_VISITANTE ?>">
                            <?php if($PublicarADM_VISITANTE == 1){ ?>
                                <figcaption> 
                                        <img class="icon-perfil" src="imagens/icon-admin.png">
                                </figcaption>
                            <?php } ?>
                        </figure>
                        
                    </a>
                <?php } else { ?>
                    <a class="link-perfil" href="<?php echo $NickUser ?>" >
                         <figure class="admin-icon">
                            <img class="imagem-post" src="<?php echo $PublicarPERFIL_VISITANTE ?>">
                            <?php if($PublicarADM_VISITANTE == 1){ ?>
                                <figcaption> 
                                        <img class="icon-perfil" src="imagens/icon-admin.png">
                                </figcaption>
                            <?php } ?>
                        </figure>
                    </a>
                <?php } ?>
                
            <?php } } else { ?>
            <section id="box-postagens">
            <img class="imagem-post" src="imagens/sem-foto.png">
            <?php } ?>
            <font color="#fc9300" face="Arial, sans-serif">

            <?php if($PublicarNICK != "ANÔNIMO"){ if($PublicarNICK != $Nick){ ?>
                <a class="link-perfil" href="<?php echo $PublicarNICK ?>" ><?php echo $PublicarNICK ?></a>
                <?php } else{ ?>
                <a class="link-perfil" href="<?php echo $PublicarNICK ?>" ><?php echo $PublicarNICK ?></a>
            <?php } } else{ ?>
                <?php echo $PublicarNICK ?>
            <?php } ?>

            - <?php echo $PublicarDATA ?> às <?php echo $PublicarHORA ?></font>
            <h1><a href="topicos?tag=<?php $TagExibirPost = substr($PublicarTAG, 1); echo $TagExibirPost; ?>"><?php echo $PublicarTAG; ?></a></h1>
            <?php 

            if($PublicarPOSTER!="" && $PublicarIMG!=""){ ?>

                <figure class="meme-legenda">
                    <?php if($dadosVisitante->rowCount() > 0 && $NickUser != $NickVisitante){ ?>
                    <a><img src="<?php echo $PublicarIMG ?>"></a>
                    <?php } else { ?>
                    <a href="poster?id=<?php echo $PublicarID ?>" ><img src="<?php echo $PublicarIMG ?>"></a>
                    <?php } ?>
                        <figcaption> 
                            <h3><?php echo $PublicarPOSTER ?></h3> 
                        </figcaption>
                </figure>
                
            <?php }else{

            if($PublicarPOSTER != "" && $PublicarVIDEO ==""){
                $chars = array("[E1]", "[E2]", "[E3]", "[E4]", "[E5]", "[E6]", "[E7]", "[E8]", "[E9]", "[E10]", "[E11]", "[E12]", "[E13]", "[E14]", "[E15]", "[E16]", "[E17]", "[E18]", "[E19]");
                $icone = array("<img src='emojis/EMOJI1.png'1>" ,"<img src='emojis/EMOJI2.png'2>" ,"<img src='emojis/EMOJI3.png'3>" ,"<img src='emojis/EMOJI4.png'4>" ,"<img src='emojis/EMOJI5.png'5>" ,"<img src='emojis/EMOJI6.png'6>" ,"<img src='emojis/EMOJI7.png'7>" ,"<img src='emojis/EMOJI8.png'8>" ,"<img src='emojis/EMOJI9.png'9>" ,"<img src='emojis/EMOJI10.png'10>" ,"<img src='emojis/EMOJI11.png'11>" ,"<img src='emojis/EMOJI12.png'12>" ,"<img src='emojis/EMOJI13.png'13>" ,"<img src='emojis/EMOJI14.png'14>" ,"<img src='emojis/EMOJI15.png'15>" ,"<img src='emojis/EMOJI16.png'16>" ,"<img src='emojis/EMOJI17.png'17>" ,"<img src='emojis/EMOJI18.png'18>" ,"<img src='emojis/EMOJI19.png'19>");
                $nova_frase = str_replace($chars, $icone, $PublicarPOSTER);
                ?>
                
                <?php if($dadosVisitante->rowCount() > 0 && $NickUser != $NickVisitante){ ?>
                <a><p><?php echo $nova_frase; ?></p></a>
                <?php } else { ?>
                <a href="poster.php?id=<?php echo $PublicarID ?>" ><p><?php echo $nova_frase; ?></p></a>
                <?php } ?>
            <?php } ?>

            <?php if($PublicarIMG != ""){ ?>
                
                <section class="img-poster">
                    <?php if($dadosVisitante->rowCount() > 0 && $NickUser != $NickVisitante){ ?>
                    <a><img src="<?php echo $PublicarIMG ?>"></a>
                    <?php } else { ?>
                    <a href="poster.php?id=<?php echo $PublicarID ?>" ><img src="<?php echo $PublicarIMG ?>"></a>
                    <?php } ?>
                </section>
                
            <?php } ?>

            <?php if($PublicarVIDEO != ""){ ?>
            <figure class="poster-video-play" >
                <?php if($dadosVisitante->rowCount() > 0 && $NickUser != $NickVisitante){ ?>
                <a>
                <?php } else { ?>
                <a href="poster?id=<?php echo $PublicarID ?>" > 
                <?php } ?>
                <video class="video-poster" id="video-poster-<?php echo $exibir['id']?>"  name="media">
                    <source src="<?php echo $PublicarVIDEO ?>" type="video/mp4">
                    </video></a>
                <figcaption>
                    
                <?php if($PublicarPOSTER != ""){ ?>
                    <h3 class="meme-legenda-video" ><?php echo $PublicarPOSTER ?></h3> 
                <?php } ?>
                    
                <script>
                 /* ------------ VIDEOS PLAY ------------ */
                function PlayVideo<?php echo $exibir['id']; ?> () {
                    document.getElementById('video-poster-<?php echo $exibir['id']; ?>').play();
                    document.getElementById('video-play-<?php echo $exibir['id']; ?>').style.display = 'none';
                    document.getElementById('video-pause-<?php echo $exibir['id']; ?>').style.display = 'block';
                }
                function PauseVideo<?php echo $exibir['id']; ?> () {
                    document.getElementById('video-poster-<?php echo $exibir['id']; ?>').pause();
                    document.getElementById('video-play-<?php echo $exibir['id']; ?>').style.display = 'block';
                    document.getElementById('video-pause-<?php echo $exibir['id']; ?>').style.display = 'none';
                }
                </script>

                <style>
                    #video-pause-<?php echo $PublicarID ?> {
                        display: none;
                    }
                    #video-play-<?php echo $PublicarID ?> {
                        display: block;
                    }
                </style>

                <a id="video-play-<?php echo $exibir['id']?>" href="javascript:PlayVideo<?php echo $exibir['id']?>();" ><img src="imagens/play-video.png" ></a>
                <a id="video-pause-<?php echo $exibir['id']?>" href="javascript:PauseVideo<?php echo $exibir['id']?>();" ><img src="imagens/pause-video.png" ></a>

                </figcaption>
            </figure>
            <?php } } ?>

            <nav class="menu-poster">
                <ul>
                    <?php if($NickUser != ""){ ?>
                    <li><a href="?funcao=gostei&id=<?php echo $PublicarID ?>"><img src="imagens/gostei-click.png"> <?php echo $PublicarGOSTEI ?></a></li>
                    <li><a href="?funcao=naogostei&id=<?php echo $PublicarID ?>"><img src="imagens/nao-gostei-click.png"> <?php echo $PublicarNAOGOSTEI ?></a></li>
                    <?php } else { ?>
                    <li><a><img src="imagens/gostei-click.png"> <?php echo $PublicarGOSTEI ?></a></li>
                    <li><a><img src="imagens/nao-gostei-click.png"> <?php echo $PublicarNAOGOSTEI ?></a></li>
                    <?php } ?>
                    <li><a class="bt-facebook" href="http://www.facebook.com/sharer/sharer.php?u=http://www.youmeme.ml/poster?id=<?php echo $PublicarID ?>" target="_blank" class="share-btn facebook"><img src="imagens/bt-facebook.png"></a></li>
                    <?php if($PublicarADM_USER == 1 || $PublicarNICK == $NickUser){ ?>
                    <li><a href="javascript:func();" onclick="excluir(<?php echo $exibir['id']; ?>)" ><img src="imagens/icon-excluir.png"></a></li>
                    <?php } ?>
                </ul>
            </nav>
                
                <script>

                function IdPost<?php echo $PublicarID ?> () {
                    document.getElementById('id-post-<?php echo $PublicarID ?>').value = '<?php echo $PublicarID ?>';
                }
                
            </script>
                
            <!-- COMENTÁRIOS -->
            <section id="box-comentarios">
                
                <?php $TodosComentariosAtual = $PDO->query("SELECT * FROM comentarios WHERE id_post='$PublicarID' "); ?>
                <h1><?php echo $TodosComentariosAtual->rowCount(); ?> comentários</h1><hr>
                
                <?php if($NickUser != ""){ ?>
                <form method="post" >
                    <a class="link-perfil" href="<?php echo $PublicarNOME_COMENTARIO ?>" >
                         <figure class="admin-icon">
                            <img class="avatar-perfil" src="<?php echo $PublicarPERFIL_USER; ?>">
                            <?php if($PublicarADM_USER == 1){ ?>
                                <figcaption> 
                                    <img class="icon-perfil" src="imagens/icon-admin.png">
                                </figcaption>
                            <?php } ?>
                        </figure>
                    </a>
                    <textarea name="box-comentar" placeholder="Comentar..." ></textarea>
                    <input class="ocultar" type="text" id="id-post-<?php echo $PublicarID ?>" name="id-post">
                    <input class="botao-comentar" type="submit" name="bt-comentar" onclick="javascript:IdPost<?php echo $PublicarID ?>();" value="COMENTAR" >
                </form>
                <?php } ?>

                <?php 

                    $TodosComentarios = $PDO->query("SELECT * FROM comentarios WHERE id_post='$PublicarID' ORDER BY id DESC LIMIT 0,3 ");

                    if($TodosComentarios->rowCount() > 0 ){

                    foreach($TodosComentarios->fetchAll() as $exibir_comentarios){
                        $PublicarID_COMENTARIO = $exibir_comentarios['id'];
                        $PublicarNOME_COMENTARIO = $exibir_comentarios['nick_comentario'];
                        $PublicarDATA_COMENTARIO = $exibir_comentarios['data']; 
                        $PublicarHORA_COMENTARIO = $exibir_comentarios['hora']; 
                        $Publicar_COMENTARIO = $exibir_comentarios['comentario']; 
                        
                        $TodosComentariosIMG = $PDO->query("SELECT * FROM usuarios WHERE nome='$PublicarNOME_COMENTARIO' ");
                        
                        foreach($TodosComentariosIMG->fetchAll() as $exibir_comentarios_img){
                            $PublicarPERFIL_COMENTARIO = $exibir_comentarios_img['perfil'];
                            $PublicarADM_COMENTARIO = $exibir_comentarios_img['ADM'];
                        
                    ?>
                
                <section class="comentarios-post">
                    <a class="link-perfil" href="<?php echo $PublicarNOME_COMENTARIO ?>" >
                         <figure class="admin-icon">
                            <img class="avatar-perfil" src="<?php echo $PublicarPERFIL_COMENTARIO; ?>">
                            <?php if($PublicarADM_COMENTARIO == 1){ ?>
                                <figcaption> 
                                    <img class="icon-perfil" src="imagens/icon-admin.png">
                                </figcaption>
                            <?php } ?>
                        </figure>
                    </a>
                    <p><a href="<?php echo $PublicarNOME_COMENTARIO ?>"><?php echo $PublicarNOME_COMENTARIO ?></a><font color="#333" size="2"> - <?php echo $PublicarDATA_COMENTARIO ?> às <?php echo $PublicarHORA_COMENTARIO ?></font><br>
                    <?php echo $Publicar_COMENTARIO; ?></p>
                    <?php if($PublicarADM == 1 || $PublicarNOME_COMENTARIO == $NickUser){ ?>
                    <a class="excluir-comentario" href="javascript:func();" onclick="excluirComentario(<?php echo $PublicarID_COMENTARIO; ?>)" ><img src="imagens/icon-excluir.png"></a>
                    <?php } ?>
                </section>
                
                <?php } } ?>
                
                <?php if($TodosComentarios->rowCount() == 3){ ?>
                <section class="link-pag-recados">
                    <?php if($NickUser != ""){ ?>
                    <a href="poster?id=<?php echo $PublicarID ?>" >CARREGAR TODOS...</a>
                    <?php } else { ?>
                    <a href="cadastrar" >CARREGAR TODOS...</a>
                    <?php } ?>
                </section><br>
                <?php } } ?>

            </section>
                
        </section>
            
    <?php
        }
    }
    else{
        ?>
            <section id="box-postagens">
                <p>Bem-vindo(a) ao YouMeme!<br>Publique seu meme ou frase e compartilhe com seus amigos!</p>
            </section>
       <?php
    }
    ?>
                
    <?php } else { ?>
       <section id="box-postagens">
            <p>Nenhum usuário encontrado!</p>
        </section>         
    <?php } ?>
                
    <footer>
        <p><a href="https://www.instagram.com/jeysson_mourasz/" target="_blank" >CEO</a> | <a href="https://www.instagram.com/marcosnemrode/" target="_blank" >COO</a> | <a href="termos-de-servico" target="_blank" >TERMOS</a> | <a href="politica-de-privacidade" target="_blank" >PRIVACIDADE</a> YouMeme © 2017</p>
    </footer>
    
</body>

</html>

<?php

    if(isset($_POST["bt-comentar"])){

        $meuidpost = $_POST["id-post"];
        $comentario = $_POST['box-comentar'];
        
        if($comentario != ""){
        
            $envarcomentario = $PDO->query("INSERT INTO comentarios (id_post, nick_comentario, data, hora, comentario) VALUES ('$meuidpost', '$NickUser' ,'$exibirdata', '$exibirhora', '$comentario' )");

            if($envarcomentario->rowCount() > 0){
                echo "<script> location.href='poster?id=$meuidpost'; </script>";
            }
            else{
                echo "<script> alert('Erro ao enviar!'); </script>";
            }
            
        }
    }

    if(isset($_POST['bt-alterar'])){
        
        $nome = ucwords($_POST['alterarnick']);
        
        $senhaAntiga = md5($_POST['senha-a']);
        $senhaNova = md5($_POST['senha-n']);
        $senhaNovaC = md5($_POST['senha-c']);
        
        $sexo = $_POST['sexo'];
        $dia = $_POST['dia'];
        $mes = $_POST['mes'];
        $ano = $_POST['ano'];
        
        if($_POST['senha-a'] != ""){
            if($senhaNova == $senhaNovaC){

                $nova_senha = $PDO->query("UPDATE usuarios SET senha='$senhaNova' WHERE nome='$NickUser' AND senha='$senhaAntiga' ");

                if($nova_senha->rowCount() > 0){
                    echo "<script> alert('Alterações realizadas com sucessso!'); location.href='$nome'; </script>";
                }else{
                    echo "<script> alert('Senha incorreta!'); location.href='$NickUser'; </script>";
                    return true;
                }

            }
            else{
                echo "<script> alert('As senhas não são iguais!'); location.href='$NickUser'; </script>";
                return true;
            }
        }
        
        $novo_nick = $PDO->query("SELECT * FROM usuarios WHERE nome='$nome' ");
        $novo_genero = $PDO->query("UPDATE usuarios SET sexo='$sexo' WHERE nome='$NickUser' ");
        $nova_idade = $PDO->query("UPDATE usuarios SET idade='$ano-$mes-$dia' WHERE nome='$NickUser' AND idade!='$ano-$mes-$dia'");

        if($novo_nick->rowCount() == 0){
            $PDO->query("UPDATE usuarios SET nome='$nome' WHERE nome='$NickUser' ");
            $PDO->query("UPDATE posters SET nick='$nome' WHERE nick='$NickUser' ");
            $PDO->query("UPDATE recados SET autor='$nome' WHERE autor='$NickUser' ");
            $PDO->query("UPDATE seguidores SET seguindo='$nome' WHERE seguindo='$NickUser' ");
            $PDO->query("UPDATE seguidores SET nickseguir='$nome' WHERE nickseguir='$NickUser' ");
            $PDO->query("UPDATE comentarios SET nick_comentario='$nome' WHERE nick_comentario='$NickUser' ");
            session_start();
            $_SESSION["Nome"] = ucwords($nome);
        }
        else{
            echo "<script> alert('NickName já existe!'); location.href='$NickUser'; </script>";
            return true;
        }
        
        if($novo_nick || $novo_genero || $nova_idade){
             echo "<script> alert('Alterações realizadas com sucessso!'); location.href='$nome'; </script>";
        }
        
    }

    if(isset($_POST['bt-enviar-img'])){
        
        $arquivoTMP = $_FILES['img-perfil-enviar']['tmp_name'];
        $nomeFile = $_FILES['img-perfil-enviar']['name'];
        
        $pExtensao = strrchr($nomeFile, '.');
        $pExtensao = strtolower($pExtensao);
        $pNovoNome = $codimg . $pExtensao;;
        $pDestino = "upload/". $pNovoNome;
        
        if($nomeFile != ""){
            
            if(move_uploaded_file($arquivoTMP, $pDestino)){

                $link_img = "./upload/" . $pNovoNome;
                $_SESSION['pURL'] = $link_img;

            }
            else{
                echo "<script> alert('Erro ao enviar!');</script> ";
            }     

            $insert_perfil = $PDO->query("UPDATE usuarios SET perfil='$link_img' WHERE nome='$NickUser' ");

            if($insert_perfil > 0){
               echo "<script> location.href='$NickUser' </script>";
            }
            
        }
        
    }

    
    if(isset($_GET["funcao"]) AND $_GET["funcao"] == "gostei"){

        $meuidgostei = $_GET["id"];

        $SelecionarGostei = $PDO->query("SELECT * FROM posters WHERE ID='$meuidgostei'");

        if($SelecionarGostei>0){
            
            $SelecionarAvalia = $PDO->query("SELECT * FROM avalia_posters WHERE id_posters='$meuidgostei$NickUser' ");
            
            if($SelecionarAvalia->rowCount() == 0){
                $PDO->query("INSERT INTO avalia_posters (id_posters, nome_gostei) VALUES ('$meuidgostei$NickUser','$NickUser') ");
                $gosteiAdd = 1;
                $naogosteiAdd = 0;
            }
            else{
                
                $VerificaAvalia = $PDO->query("SELECT * FROM avalia_posters WHERE nome_gostei='$NickUser' AND nome_naogostei='' AND id_posters='$meuidgostei$NickUser' ");
                
                if($VerificaAvalia->rowCount() == 0){
                    $PDO->query("UPDATE avalia_posters SET nome_gostei='$NickUser', nome_naogostei='' WHERE id_posters='$meuidgostei$NickUser' ");
                    $gosteiAdd = 1;
                    $naogosteiAdd = -1;
                }
                else{ 
                    $PDO->query("DELETE FROM avalia_posters WHERE id_posters='$meuidgostei$NickUser' ");
                    $gosteiAdd = -1;
                    $naogosteiAdd = 0;
                }

            }
            
            foreach($SelecionarGostei->fetchAll() as $pegarPoster){
                $GosteiAtual = $pegarPoster['gostei'] += $gosteiAdd;
                $inserirGostei = $PDO->query("UPDATE posters SET gostei='$GosteiAtual' WHERE id='$meuidgostei'");
                
                $NaoGosteiAtual = $pegarPoster['naogostei'] += $naogosteiAdd;
                $inserirNaoGostei = $PDO->query("UPDATE posters SET naogostei='$NaoGosteiAtual' WHERE id='$meuidgostei'");
            }
            
            echo "<script> location.href='$NickUser'; </script>"; 

        }

    }

    if(isset($_GET["funcao"]) AND $_GET["funcao"] == "naogostei"){

        $meuidnaogostei = $_GET["id"];

        $SelecionarNaoGostei = $PDO->query("SELECT * FROM posters WHERE ID='$meuidnaogostei'"); 

        if($SelecionarNaoGostei>0){
            
            $SelecionarAvalia = $PDO->query("SELECT * FROM avalia_posters WHERE id_posters='$meuidnaogostei$NickUser' ");
            
            if($SelecionarAvalia->rowCount() == 0){
                $PDO->query("INSERT INTO avalia_posters (id_posters, nome_naogostei) VALUES ('$meuidnaogostei$NickUser','$NickUser') ");
                $gosteiAdd = 0;
                $naogosteiAdd = 1;
            }
            else{
                
                $VerificaAvalia = $PDO->query("SELECT * FROM avalia_posters WHERE nome_gostei='' AND nome_naogostei='$NickUser' AND id_posters='$meuidnaogostei$NickUser' ");
                
                if($VerificaAvalia->rowCount() == 0){
                    $PDO->query("UPDATE avalia_posters SET nome_gostei='', nome_naogostei='$NickUser' WHERE id_posters='$meuidnaogostei$NickUser' ");
                    $gosteiAdd = -1;
                    $naogosteiAdd = 1;
                }
                else{
                    $PDO->query("DELETE FROM avalia_posters WHERE id_posters='$meuidnaogostei$NickUser' ");
                    $gosteiAdd = 0;
                    $naogosteiAdd = -1;
                }

            }
            
            foreach($SelecionarNaoGostei->fetchAll() as $pegarPoster){
                $GosteiAtual = $pegarPoster['gostei'] += $gosteiAdd;
                $inserirGostei = $PDO->query("UPDATE posters SET gostei='$GosteiAtual' WHERE id='$meuidnaogostei'");
                
                $NaoGosteiAtual = $pegarPoster['naogostei'] += $naogosteiAdd;
                $inserirNaoGostei = $PDO->query("UPDATE posters SET naogostei='$NaoGosteiAtual' WHERE id='$meuidnaogostei'");
            }
            
            echo "<script> location.href='$NickUser'; </script>"; 

        }

    }

?>